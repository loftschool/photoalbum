<!--/--------- встплывашка для добавления "показать еще" ---------\-->

<script id="project-template" type="text/x-handlebars-template">
    @{{#each photos}}
        <li class="projects__item">
            <a href="#pic-view" style="background-image:url(@{{ thumb }})" class="projects__pic show-latest" data-id="@{{ id }}"></a>
            <div class="projects__desc">
                <div class="project-info">
                    <div class="project-info__pic">
                        <a href="/user/@{{ user.id }}" style="background-image:url(@{{ user.photo }})" class="project-info__user"></a>
                    </div>
                    <div class="project-info__desc">
                        <div class="project-info__title">
                            @{{ name }}
                        </div>
                        <div class="project-activity">
                            <a href="#" class="project-activity__elem">
                                <div class="project-activity__icon">
                                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 17.21">
                                        <path class="cls-1" d="M256.15,247.4c-5.52-.11-10.07,3.29-10.15,7.6a6.55,6.55,0,0,0,1.4,4.11h0c1.57,2-.14,5.49-0.14,5.49l5.07-2.18a12.53,12.53,0,0,0,3.52.57c5.52,0.11,10.07-3.29,10.15-7.6s-4.32-7.89-9.85-8m-4.43,9A1.23,1.23,0,1,1,253,255.2a1.23,1.23,0,0,1-1.23,1.23m4.11,0a1.23,1.23,0,1,1,1.23-1.23,1.23,1.23,0,0,1-1.23,1.23m4.11,0a1.23,1.23,0,1,1,1.23-1.23,1.23,1.23,0,0,1-1.23,1.23" transform="translate(-246 -247.39)"/>
                                    </svg>
                                </div>
                                <div class="project-activity__counter">@{{ Comments_count }}</div>
                            </a>
                            <a href="#" class="project-activity__elem">
                                <div class="project-activity__icon">
                                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 16.86">
                                        <path class="cls-1" d="M74,60.86a5.28,5.28,0,0,0-10-2.39,5.28,5.28,0,0,0-10,2.39c0,5.52,8.87,11.57,10,11.57S74,66.41,74,60.86Z" transform="translate(-54 -55.57)"/>
                                    </svg>
                                </div>
                                <div class="project-activity__counter">@{{ Likes_count }}</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="projects__extra">
                <div class="project-extra">
                    <div class="project-extra__icon">
                        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 18.18">
                            <g id="miu">
                                <g id="Artboard-1">
                                    <path id="editor-album-collection-glyph" d="M2.13,6.55A0.12,0.12,0,0,0,2,6.66V21a0.12,0.12,0,0,0,.13.12H21.87A0.12,0.12,0,0,0,22,21V6.66a0.12,0.12,0,0,0-.13-0.12H2.13ZM3.82,2.91V3.82H20.18V2.91H3.82ZM2.91,4.73V5.64H21.09V4.73H2.91Z" transform="translate(-2 -2.91)"/>
                                </g>
                            </g>
                        </svg>
                    </div>
                    <div class="project-extra__desc"><a href="/album/@{{ album.id }}">@{{ album.name }}</a></div>
                </div>
            </div>
        </li>
    @{{/each}}
</script>

<!--/--------- (просмотр фотографии) - большая всплывашка на главной  ---------\-->
<script id="album-template" type="text/x-handlebars-template">
    <div class="pic-view__gallery">
        <ul class="pic-view__images owl-carousel">
            @{{#each items}}
                <li class="pic-view__image" data-id="@{{ id }}"><img src="@{{ url }}" alt=""></li>
            @{{/each }}
        </ul>
        <div class="pic-view__gallery-controls">
            <a href="#" class="pic-view__gallery-btn pic-view__gallery-btn_right">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="15.844px" height="25.859px" viewBox="0 0 15.844 25.859"
                     enable-background="new 0 0 15.844 25.859" xml:space="preserve">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M15.826,12.841c0.012-0.662-0.273-1.389-0.849-1.964
                        c-0.396-0.396-0.865-0.652-1.335-0.773L4.039,0.496C3.257-0.285,1.833-0.126,0.858,0.85c-0.977,0.977-1.135,2.401-0.354,3.182
                        l8.881,8.884l-8.893,8.896c-0.779,0.779-0.616,2.207,0.364,3.189c0.981,0.98,2.409,1.145,3.188,0.363l9.599-9.602
                        c0.476-0.116,0.95-0.371,1.351-0.771C15.624,14.363,15.908,13.55,15.826,12.841z"/>
                </svg>
            </a>
            <a href="#" class="pic-view__gallery-btn pic-view__gallery-btn_left">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="15.969px" height="26.015px" viewBox="0 0 15.969 26.015"
                     enable-background="new 0 0 15.969 26.015" xml:space="preserve">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M15.472,21.943L6.51,12.994l8.952-8.938
                            c0.787-0.786,0.627-2.22-0.356-3.201c-0.983-0.982-2.419-1.142-3.206-0.356L2.22,10.164c-0.474,0.122-0.946,0.379-1.346,0.778
                            c-0.58,0.579-0.867,1.31-0.854,1.976c-0.083,0.713,0.203,1.531,0.837,2.164c0.403,0.402,0.881,0.658,1.36,0.776l9.675,9.661
                            c0.787,0.784,2.225,0.62,3.214-0.367S16.258,22.728,15.472,21.943z"/>
                </svg>
            </a>
        </div>
    </div>
    <div class="pic-view__desc">
        <div class="pic-view__info">
            <div class="user-present" id="user-ident">
                <div style="background-image: url('@{{ userphoto }}')"
                     class="user-present__avatar user-avatar"></div>
                <div class="user-present__username">@{{ username }}</div>
            </div>
            <div class="likes-container">
                @{{#if liked}}
                    <a href="#" class="likes liked">
                        <div class="likes__icon">
                            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 23 19.86">
                                <path class="cls-1"
                                      d="M224.07,60.86a5.28,5.28,0,0,0-10-2.39,5.28,5.28,0,0,0-10,2.39c0,5.52,8.87,11.57,10,11.57S224.07,66.41,224.07,60.86Z"
                                      transform="translate(-202.57 -54.07)"/>
                            </svg>
                        </div>
                        <div class="likes__amount" id="likes-display">@{{ likesAmount }}</div>
                    </a>
                @{{ else }}
                    <a href="#" class="likes">
                        <div class="likes__icon">
                            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 23 19.86">
                                <path class="cls-1"
                                      d="M224.07,60.86a5.28,5.28,0,0,0-10-2.39,5.28,5.28,0,0,0-10,2.39c0,5.52,8.87,11.57,10,11.57S224.07,66.41,224.07,60.86Z"
                                      transform="translate(-202.57 -54.07)"/>
                            </svg>
                        </div>
                        <div class="likes__amount" id="likes-display">@{{ likesAmount }}</div>
                    </a>
                @{{/if}}
            </div>
        </div>
        <div class="pic-view__about" id="photo-about">
            <div class="pic-view__about-title">
                @{{ title }}
            </div>
            <div class="pic-view__about-desc">
                @{{{ desc }}}
            </div>
        </div>
    </div>
    <div class="pic-view__comments">
        <div class="comments">
            <div class="comment__header"><a href="#" class="comments__trigger">Комментарии</a></div>
            <div class="comments__body">
                <div class="comments__item">
                    <div class="comments__avatar">
                        <div style="background-image: url('{{$userdata->photo or '/img/no_photo.jpg'}}')"
                             class="comments__avatar-pic user-avatar"></div>
                    </div>
                    <div class="comments__content">
                        <div class="comments__username">{{$userdata->name or ''}}</div>
                        <div class="comments__add-form">
                            <form class="comments__add-form-elem" action="/api/newComment/" id="add-comment">
                                <input type="hidden" name="photo-id" id="photo-id" value="@{{ photoId }}">
                                <input name="comment" type="text" placeholder="Добавить комментарий"
                                       class="comments__add-input input-text input-text_bordered">
                                <button type="submit" class="comments__add-submit round-btn round-btn_bordered">
                                    Добавить
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                <ul class="comments__list">
                    @{{#each comments}}
                        <li class="comments__item">
                            <div class="comments__avatar">
                                <div style="background-image: url('@{{ user.photo }}')"
                                     class="comments__avatar-pic user-avatar"></div>
                            </div>
                            <div class="comments__content">
                                <div class="comments__username">@{{ user.name }}</div>
                                <div class="comments__text">
                                    <p>
                                        @{{ comment }}
                                    </p>
                                </div>
                            </div>
                        </li>
                    @{{/each}}
                </ul>
            </div>
        </div>
    </div>
</script>

<!--/--------- описание фотографии ---------\-->

<script id="description-template" type="text/x-handlebars-template">
    <div class="pic-view__about-title">
        @{{ title }}
    </div>
    <div class="pic-view__about-desc">
        @{{{ desc }}}
    </div>
</script>

<!--/--------- инфо юзера ---------\-->
<script id="user-info" type="text/x-handlebars-template">
    <div style="background-image: url('@{{ userphoto }}')"
         class="user-present__avatar user-avatar"></div>
    <div class="user-present__username">@{{ username }}</div>
</script>

<!--/--------- шаблон албома при добавлении ---------\-->

<script type="text/x-handlebars-template" id="album-item-template">
    <li class="projects__item" data-id="@{{ id }}">
        <div style="background-image:url('@{{ photo }}')" class="projects__pic show-page">
            <div class="project__pic-desc">
                <div class="project__pic-desc-text">
                    @{{ name }}
                </div>
                <div class="project__pic-desc-photos">0 фотографий</div>
            </div>
        </div>
        <div class="projects__desc">
            <div class="project-extra">
                <a href="#photo-edit" data-params="description=@{{ description }}&name=@{{ name }}&id=@{{ id }}" class="project-extra__icon show-edit">
                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 17">
                        <title>pen</title>
                        <path d="M21.63,6.37a11.55,11.55,0,0,0-4-4L19,1a4.94,4.94,0,0,1,3,1,4.94,4.94,0,0,1,1,3ZM10,18H6V14l0.48-.48a8.38,8.38,0,0,1,4,4ZM20.48,7.52l-8.85,8.85a11.61,11.61,0,0,0-1.75-2.23,11.57,11.57,0,0,0-2.27-1.75l8.86-8.86A8.37,8.37,0,0,1,20.48,7.52Z" transform="translate(-6 -1)"/>
                    </svg>
                </a>
                <div class="project-extra__desc"><a href="/album/@{{ id }}">@{{ name }}</a></div>
            </div>
        </div>
    </li>
</script>

<!--/--------- комменты ---------\-->
<script id="comments-template" type="text/x-handlebars-template">
    <li class="comments__item">
        <div class="comments__avatar">
            <div style="background-image: url('@{{ userphoto }}')" class="comments__avatar-pic user-avatar"></div>
        </div>
        <div class="comments__content">
            <div class="comments__username">@{{ username }}</div>
            <div class="comments__text">
                <p>
                    @{{ comment }}
                </p>
            </div>
        </div>
    </li>
</script>

<!--/--------- лайки ---------\-->

<script id="like-template" type="text/x-handlebars-template">
    @{{#if liked}}
        <a href="#" class="likes liked">
            <div class="likes__icon">
                <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg"
                     viewBox="0 0 23 19.86">
                    <path class="cls-1"
                          d="M224.07,60.86a5.28,5.28,0,0,0-10-2.39,5.28,5.28,0,0,0-10,2.39c0,5.52,8.87,11.57,10,11.57S224.07,66.41,224.07,60.86Z"
                          transform="translate(-202.57 -54.07)"/>
                </svg>
            </div>
            <div class="likes__amount" id="likes-display">@{{ likesAmount }}</div>
        </a>
    @{{ else }}
        <a href="#" class="likes">
            <div class="likes__icon">
                <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg"
                     viewBox="0 0 23 19.86">
                    <path class="cls-1"
                          d="M224.07,60.86a5.28,5.28,0,0,0-10-2.39,5.28,5.28,0,0,0-10,2.39c0,5.52,8.87,11.57,10,11.57S224.07,66.41,224.07,60.86Z"
                          transform="translate(-202.57 -54.07)"/>
                </svg>
            </div>
            <div class="likes__amount" id="likes-display">@{{ likesAmount }}</div>
        </a>
    @{{/if}}
</script>

<!--/--------- фото в загрузчике ---------\-->
<script id="photo-template" type="text/x-handlebars-template">
    <li class="fileupload__photos-item" data-id="@{{ ident }}">
        <div class="fileupload__photo-pic" style="background-image: url(@{{ cover }})">
            @{{#if removeBtn }}
                <div class="fileupload__photo-remove">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         width="26.392px" height="26.015px" viewBox="0 0 26.392 26.015" enable-background="new 0 0 26.392 26.015" xml:space="preserve">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M25.896,21.943l-8.963-8.949l8.951-8.938
                            c0.787-0.786,0.628-2.219-0.355-3.201c-0.984-0.982-2.42-1.142-3.206-0.356l-9.181,9.167L4.039,0.558
                            C3.257-0.223,1.833-0.064,0.858,0.912c-0.977,0.977-1.135,2.401-0.354,3.182l8.881,8.884l-8.893,8.896
                            c-0.779,0.779-0.616,2.207,0.364,3.189c0.981,0.98,2.409,1.145,3.188,0.363l9.081-9.083l9.188,9.175
                            c0.786,0.784,2.225,0.62,3.214-0.367C26.517,24.165,26.682,22.728,25.896,21.943z"/>
                    </svg>
                </div>
            @{{/if}}
        </div>
    </li>
</script>


<!--/--------- фото в альбоме ---------\-->
<script id="photo-item-template" type="text/x-handlebars-template">
    <li class="projects__item projects__item_inner projects__item_newbie" data-id="@{{ id }}">
        <a href="#pic-view" data-photo-id="@{{ id }}" data-id="{{$album->id or ''}}" style="background-image:url('@{{ cover }}')" class="projects__pic show-album">
            <div class="project-activity">
                <div class="project-activity__elem">
                    <div class="project-activity__icon">
                        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 17.21">
                            <path class="cls-1" d="M256.15,247.4c-5.52-.11-10.07,3.29-10.15,7.6a6.55,6.55,0,0,0,1.4,4.11h0c1.57,2-.14,5.49-0.14,5.49l5.07-2.18a12.53,12.53,0,0,0,3.52.57c5.52,0.11,10.07-3.29,10.15-7.6s-4.32-7.89-9.85-8m-4.43,9A1.23,1.23,0,1,1,253,255.2a1.23,1.23,0,0,1-1.23,1.23m4.11,0a1.23,1.23,0,1,1,1.23-1.23,1.23,1.23,0,0,1-1.23,1.23m4.11,0a1.23,1.23,0,1,1,1.23-1.23,1.23,1.23,0,0,1-1.23,1.23" transform="translate(-246 -247.39)"/>
                        </svg>
                    </div>
                    <div class="project-activity__counter">0</div>
                </div>
                <div class="project-activity__elem">
                    <div class="project-activity__icon">
                        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 16.86">
                            <path class="cls-1" d="M74,60.86a5.28,5.28,0,0,0-10-2.39,5.28,5.28,0,0,0-10,2.39c0,5.52,8.87,11.57,10,11.57S74,66.41,74,60.86Z" transform="translate(-54 -55.57)"/>
                        </svg>
                    </div>
                    <div class="project-activity__counter">0</div>
                </div>
            </div>
        </a>
        <div class="projects__desc">
            <div class="project-extra">
                <a href="#photo-edit" data-params="description=&name=Фотография без описания&id=@{{ id }}" class="project-extra__icon show-edit">
                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 17">
                        <title>pen</title>
                        <path d="M21.63,6.37a11.55,11.55,0,0,0-4-4L19,1a4.94,4.94,0,0,1,3,1,4.94,4.94,0,0,1,1,3ZM10,18H6V14l0.48-.48a8.38,8.38,0,0,1,4,4ZM20.48,7.52l-8.85,8.85a11.61,11.61,0,0,0-1.75-2.23,11.57,11.57,0,0,0-2.27-1.75l8.86-8.86A8.37,8.37,0,0,1,20.48,7.52Z" transform="translate(-6 -1)"/>
                    </svg>
                </a>
                <div class="project-extra__desc">
                    <a>
                       Фотография без названия
                    </a>
                </div>
            </div>
        </div>
    </li>
</script>

<!--/--------- уведомление типа confirm ---------\-->
<script type="text/x-handlebars-template" id="popup-alert">
    <div class="popup-alert">
        <div class="popup-alert__box">
            <div class="popup-alert__title">
                @{{ text }}
            </div>
            <div class="popup-alert__btns">
                <a href="#yes" class="round-btn round-btn_alerted popup-alert-btn">
                    Да
                </a>
                <a href="#no" class="round-btn round-btn_alerted popup-alert-btn">
                    Отменить
                </a>
            </div>
        </div>
    </div>
</script>

