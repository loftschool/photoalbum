@extends('layouts.internalpage')
@section('content')
<div class="page-content">
    <section class="new section">
        <div class="container">
            <div class="section__title">Новое в мире</div>
            @if(count($photos) > 0)
            <ul class="projects">
                @foreach($photos as $photo)
                <li class="projects__item">
                    <a href="#pic-view" style="background-image:url('{{$photo->thumb or '/img/content/project.jpg'}}')" class="projects__pic show-latest" data-id="{{$photo->id}}"></a>
                    <div class="projects__desc">
                        <div class="project-info">
                            <div class="project-info__pic">
                                <a href="/user/{{$photo->user->id}}" style="background-image:url('{{$photo->user->photo or '/img/no_photo.jpg'}}')" class="project-info__user"></a></div>
                            <div class="project-info__desc">
                                <div class="project-info__title">{{$photo->name or ''}}</div>
                                <div class="project-activity"><a href="#" class="project-activity__elem">
                                        <div class="project-activity__icon">
                                            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 17.21">
                                                <path class="cls-1" d="M256.15,247.4c-5.52-.11-10.07,3.29-10.15,7.6a6.55,6.55,0,0,0,1.4,4.11h0c1.57,2-.14,5.49-0.14,5.49l5.07-2.18a12.53,12.53,0,0,0,3.52.57c5.52,0.11,10.07-3.29,10.15-7.6s-4.32-7.89-9.85-8m-4.43,9A1.23,1.23,0,1,1,253,255.2a1.23,1.23,0,0,1-1.23,1.23m4.11,0a1.23,1.23,0,1,1,1.23-1.23,1.23,1.23,0,0,1-1.23,1.23m4.11,0a1.23,1.23,0,1,1,1.23-1.23,1.23,1.23,0,0,1-1.23,1.23" transform="translate(-246 -247.39)"/>
                                            </svg>

                                        </div>
                                        <div class="project-activity__counter">{{$photo->Comments_count}}</div></a><a href="#" class="project-activity__elem">
                                        <div class="project-activity__icon"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 16.86">
                                                <path class="cls-1" d="M74,60.86a5.28,5.28,0,0,0-10-2.39,5.28,5.28,0,0,0-10,2.39c0,5.52,8.87,11.57,10,11.57S74,66.41,74,60.86Z" transform="translate(-54 -55.57)"/>
                                            </svg>

                                        </div>
                                        <div class="project-activity__counter">{{$photo->Likes_count}}</div></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="projects__extra">
                        <div class="project-extra">
                            <div class="project-extra__icon"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 18.18">
                                    <title>folders</title>
                                    <g id="miu">
                                        <g id="Artboard-1">
                                            <path id="editor-album-collection-glyph" d="M2.13,6.55A0.12,0.12,0,0,0,2,6.66V21a0.12,0.12,0,0,0,.13.12H21.87A0.12,0.12,0,0,0,22,21V6.66a0.12,0.12,0,0,0-.13-0.12H2.13ZM3.82,2.91V3.82H20.18V2.91H3.82ZM2.91,4.73V5.64H21.09V4.73H2.91Z" transform="translate(-2 -2.91)"/>
                                        </g>
                                    </g>
                                </svg>

                            </div>
                            <div class="project-extra__desc"><a href="/album/{{$photo->album->id or ''}}">{{$photo->album->name or ''}}</a></div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
                @else
                <div class="projects__empty-alert">Увы, пока что ничего не загружено. Загрузите что-нибудь и станьте первым!</div>
                <ul class="projects"></ul>
            @endif
            <div class="projects__empty-alert"></div>
            @if(count($photos) >= 6)
            <div class="proceed"><a href="#" class="proceed__btn round-btn round-btn_bordered">Показать еще</a></div>
            @endif
        </div>
    </section>
    <section class="section albums">
        <div class="container">
            <div class="albums__add-btn">
                <a href="#add-album" class="edit-btn edit-btn_add">
                    <div class="edit-btn__inner">
                        <div class="edit-btn__icon">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 width="20px" height="20px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve">
                        <path d="M12.448,17.552C12.448,18.904,11.352,20,10,20l0,0c-1.352,0-2.447-1.096-2.447-2.448V2.448
                            C7.553,1.096,8.648,0,10,0l0,0c1.352,0,2.448,1.096,2.448,2.448V17.552z"/>
                                <path d="M17.553,7.552C18.904,7.552,20,8.648,20,10l0,0c0,1.351-1.096,2.448-2.447,2.448H2.449
                            C1.096,12.448,0,11.352,0,10l0,0c0-1.353,1.096-2.449,2.449-2.449H17.553z"/>
                    </svg>
                        </div>
                        <div class="edit-btn__text">Добавить</div>
                    </div>
                </a>
            </div>
            <div class="section__title">Мои альбомы</div>
            @if(count($myalbums)>0)
            <ul class="projects projects_albums">
                @foreach($myalbums as $album)
                <li class="projects__item" data-id="{{$album->id}}">
                    <a href="/album/{{$album->id}}" style="background-image:url('{{$album->photo->thumb or '/img/content/project.jpg'}}')" class="projects__pic show-page">
                        <div class="project__pic-desc">
                            <div class="project__pic-desc-text">
                                {{$album->photo->name or ''}}
                            </div>
                            <div class="project__pic-desc-photos">{{$album->Photo_Count or '0'}} фотографий</div>
                        </div>
                    </a>
                    <div class="projects__desc">
                        <div class="project-extra">
                            <a href="#photo-edit" data-params="description={{$album->description}}&name={{$album->name}}&id={{$album->id}}" class="project-extra__icon show-edit">
                                <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 17">
                                    <title>pen</title>
                                    <path d="M21.63,6.37a11.55,11.55,0,0,0-4-4L19,1a4.94,4.94,0,0,1,3,1,4.94,4.94,0,0,1,1,3ZM10,18H6V14l0.48-.48a8.38,8.38,0,0,1,4,4ZM20.48,7.52l-8.85,8.85a11.61,11.61,0,0,0-1.75-2.23,11.57,11.57,0,0,0-2.27-1.75l8.86-8.86A8.37,8.37,0,0,1,20.48,7.52Z" transform="translate(-6 -1)"/>
                                </svg>
                            </a>
                            <div class="project-extra__desc"><a href="/album/{{$album->id}}">{{$album->name}}</a></div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
            @else
            <div class="projects__empty-alert">
                Альбомы еще не созданы, создайте альбом с помощью кнопки добавить
            </div>
                <ul class="projects projects_albums"></ul>
            @endif
        </div>
    </section>
</div>
@endsection

@section('popups')
    @include('popups')
    @include('handlebars')
@endsection

@section('title')
    Фото Альбом - {{$user->name}}
@endsection

