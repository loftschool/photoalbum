@extends('layouts.main')
@section('content')
    <div class="auth">
        <div class="auth__block">
            <div class="auth__block-flip">
                <div class="auth__block-side auth__block-front">
                    <div class="auth__text">
                        <div class="auth__text-title">Восстановление пароля</div>
                        <div class="auth__text-desc">Введите E-mail для восстановления пароля</div>
                    </div>
                    <div class="auth__form">
                        <form class="auth__elem" method="POST" action="{{ url('/password/email') }}">

                            {{csrf_field()}}
                            <label class="auth__row">
                                <div class="auth__row-icon"><svg id="Mail" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 109.29 71.16">
                                        <title>env</title>
                                        <path d="M54.65,29.07L101.06,0.13A9.8,9.8,0,0,0,99.52,0H9.77A10,10,0,0,0,8.23.13Z"/>
                                        <path d="M57.57,40.26l-0.19.1-0.21.11a5.47,5.47,0,0,1-1.12.44l-0.12,0a5.59,5.59,0,0,1-1.27.16h0a5.53,5.53,0,0,1-1.27-.16l-0.12,0a5.65,5.65,0,0,1-1.12-.44l-0.21-.11-0.2-.1L0.15,8.1A9.9,9.9,0,0,0,0,9.77V61.38a9.77,9.77,0,0,0,9.77,9.77H99.52a9.77,9.77,0,0,0,9.77-9.77V9.77a9.71,9.71,0,0,0-.15-1.67Z"/>
                                    </svg>

                                </div>
                                <div class="auth__row-input">
                                    <input name="email" type="text" placeholder="Электронная почта" class="auth__input" value="{{ old('email') }}">
                                </div>
                            </label>
                            @if (count($errors) > 0)
                                <div class="errors-container">
                                    <div class="popup-errors">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            @endif
                            @if (session('status'))
                                <div class="errors-container">
                                    <div class="popup-errors popup-errors_success">
                                        <ul>
                                            <li>{{ session('status') }}</li>
                                        </ul>
                                    </div>
                                </div>
                            @endif
                            <div class="auth__extras">
                                <a href="/" class="auth__forgot auth__link">Авторизация</a>
                                <input name="" type="submit" value="Выслать пароль" class="auth__enter round-btn round-btn_filled">
                                <div class="auth__remember"><span class="auth__remember-text">Нет аккаунта?</span>
                                    <a href="/#register" class="auth__register-link auth__link flip-trigger">Зарегистрироваться</a></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer-auth">2016 Создано командой профессионалов на продвинутом курсе по веб-разработке от LoftSchool</footer>
@endsection


@section('head')
    <link rel="stylesheet" href="/css/plugins.auth.min.css">
@endsection

@section('wrapper')
    wrapper_auth
@endsection

