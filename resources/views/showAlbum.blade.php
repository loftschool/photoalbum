@extends('layouts.main')
@section('header')
    <div class="fixed-header">
        <div class="fixed-header__inner">
            <div class="container">
                <a href="/" class="header-inner__user">
                    <div style="background-image: url('{{$user->photo or ''}}')" class="user-avatar" id="user-pic"></div>
                    <div class="header-inner__user-name" id="user-name">{{$user->name or ''}}</div>
                </a>
                <div class="project-extra__desc">
                    {{$album->name}}
                </div>
                <a href="/" class="edit-btn header__go-home">
                    <div class="edit-btn__inner">
                        <div class="edit-btn__icon">
                            <svg version="1.1" id="Home" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         width="20px" height="21.696px" viewBox="59.499 58.652 20 21.696" enable-background="new 59.499 58.652 20 21.696"
                                                         xml:space="preserve">
                                <path d="M79.116,66.476l-9.058-7.619c-0.326-0.274-0.801-0.274-1.127,0l-9.121,7.673
                                c-0.282,0.237-0.385,0.625-0.259,0.97c0.126,0.346,0.455,0.576,0.823,0.576h1.331v10.171c0,1.16,0.941,2.101,2.101,2.101h2.976
                                v-5.952c0-0.386,0.313-0.7,0.7-0.7h4.026c0.387,0,0.7,0.314,0.7,0.7v5.952h2.977c1.16,0,2.101-0.94,2.101-2.101V68.076h1.33
                                c0.003,0,0.006,0,0.008,0c0.483,0,0.875-0.392,0.875-0.875C79.499,66.899,79.347,66.633,79.116,66.476z"/>
                            </svg>
                        </div>
                        <div class="edit-btn__text">На главную</div>
                    </div>
                </a>
            </div>
            <div class="container">
                <ul class="stats__list">
                    <li class="stats__item">
                        <div class="stats__num photo-counter">{{$album->photoCount}}</div>
                        <div class="stats__type">Фотографий</div>
                    </li>
                    <li class="stats__item">
                        <div class="stats__num likes-counter">{{$likes}}</div>
                        <div class="stats__type">Лайков</div>
                    </li>
                    <li class="stats__item">
                        <div class="stats__num comments-counter">{{$comments}}</div>
                        <div class="stats__type">Комментариев</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <header style="background-image:url('{{$album->photo->url}}')" class="header header-inner">
        <div class="header__alerts" id="header-alerts">
            <ul class="header__alerts-content"></ul>
        </div>
        <div class="header__content">
            <div class="container header-container">
                @if(Auth::id() == $album->user_id)
                    <a href="#" class="edit-btn header__edit-btn">
                        <div class="edit-btn__inner">
                            <div class="edit-btn__icon"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 17">
                                    <title>pen</title>
                                    <path d="M21.63,6.37a11.55,11.55,0,0,0-4-4L19,1a4.94,4.94,0,0,1,3,1,4.94,4.94,0,0,1,1,3ZM10,18H6V14l0.48-.48a8.38,8.38,0,0,1,4,4ZM20.48,7.52l-8.85,8.85a11.61,11.61,0,0,0-1.75-2.23,11.57,11.57,0,0,0-2.27-1.75l8.86-8.86A8.37,8.37,0,0,1,20.48,7.52Z" transform="translate(-6 -1)"/>
                                </svg>

                            </div>
                            <div class="edit-btn__text">Редактировать</div>
                        </div></a>
                @endif
                <a href="/" class="edit-btn header__go-home">
                    <div class="edit-btn__inner">
                        <div class="edit-btn__icon"><svg version="1.1" id="Home" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         width="20px" height="21.696px" viewBox="59.499 58.652 20 21.696" enable-background="new 59.499 58.652 20 21.696"
                                                         xml:space="preserve">
                            <path d="M79.116,66.476l-9.058-7.619c-0.326-0.274-0.801-0.274-1.127,0l-9.121,7.673
                            c-0.282,0.237-0.385,0.625-0.259,0.97c0.126,0.346,0.455,0.576,0.823,0.576h1.331v10.171c0,1.16,0.941,2.101,2.101,2.101h2.976
                            v-5.952c0-0.386,0.313-0.7,0.7-0.7h4.026c0.387,0,0.7,0.314,0.7,0.7v5.952h2.977c1.16,0,2.101-0.94,2.101-2.101V68.076h1.33
                            c0.003,0,0.006,0,0.008,0c0.483,0,0.875-0.392,0.875-0.875C79.499,66.899,79.347,66.633,79.116,66.476z"/>
                            </svg>
                        </div>
                        <div class="edit-btn__text">На главную</div>
                    </div>
                </a>
                <div class="header-inner__header">
                    <a href="/" class="header-inner__user">
                        <div style="background-image: url('{{$user->photo or ''}}')" class="user-avatar"></div>
                        <div class="header-inner__user-name">{{$user->name or ''}}</div>
                    </a>
                </div>
                <div class="header-inner__container">
                    <div class="header-inner__text">
                        <div class="header-inner__title">{{$album->name}}</div>
                        <div class="header-inner__desc">
                            {{$album->description}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header__bar">
            <div class="header__bar-inner">
                <div class="container fixed-content">
                    <div class="header-inner__user">
                        <a href="/" style="background-image: url('{{$user->photo or ''}}')" class="user-avatar"></a>
                        <div class="header-inner__user-name">{{$user->name or ''}}</div>
                    </div>
                    <div class="project-extra__desc">
                        {{$album->name}}
                    </div>
                    <a href="/" class="edit-btn header__go-home">
                        <div class="edit-btn__inner">
                            <div class="edit-btn__icon">
                                <svg version="1.1" id="Home" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="20px" height="21.696px" viewBox="59.499 58.652 20 21.696" enable-background="new 59.499 58.652 20 21.696"
                                     xml:space="preserve">
                                <path d="M79.116,66.476l-9.058-7.619c-0.326-0.274-0.801-0.274-1.127,0l-9.121,7.673
                                c-0.282,0.237-0.385,0.625-0.259,0.97c0.126,0.346,0.455,0.576,0.823,0.576h1.331v10.171c0,1.16,0.941,2.101,2.101,2.101h2.976
                                v-5.952c0-0.386,0.313-0.7,0.7-0.7h4.026c0.387,0,0.7,0.314,0.7,0.7v5.952h2.977c1.16,0,2.101-0.94,2.101-2.101V68.076h1.33
                                c0.003,0,0.006,0,0.008,0c0.483,0,0.875-0.392,0.875-0.875C79.499,66.899,79.347,66.633,79.116,66.476z"/>
                            </svg>
                            </div>
                            <div class="edit-btn__text">На главную</div>
                        </div>
                    </a>
                </div>
                <div class="container">
                    <ul class="stats__list">
                        <li class="stats__item">
                            <div class="stats__num photo-counter">{{$album->photoCount}}</div>
                            <div class="stats__type">Фотографий</div>
                        </li>
                        <li class="stats__item">
                            <div class="stats__num likes-counter">{{$likes}}</div>
                            <div class="stats__type">Лайков</div>
                        </li>
                        <li class="stats__item">
                            <div class="stats__num comments-counter">{{$comments}}</div>
                            <div class="stats__type">Комментариев</div>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="edit-bar">
                <div class="container">
                    <a href="#" class="edit-bar__btn edit-bar__btn_cancel round-btn round-btn_blank">Отменить</a>
                    <a href="#" class="edit-bar__btn round-btn round-btn_filled" data-album="{{ $album->id }}" id="save-header">Сохранить</a></div>
            </div>
        </div>
        <div class="user-edit">
            <div class="photo-edit photo-edit_static">
                <label class="photo-edit__link">
                    <input name="" type="file" value="" class="photo-edit__file">
                    <div class="photo-edit__decor">
                        <div class="photo-edit__icon">
                            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 416 320">
                                <g>
                                    <path d="M430.4,147H362.9l-40.4-40.8a0.77,0.77,0,0,0-.3-0.2l-0.2-.2h0A32.71,32.71,0,0,0,298.7,96h-84a33.07,33.07,0,0,0-24.6,10.9V107l-39.5,40h-69A33.21,33.21,0,0,0,48,180.2V382.3A33.63,33.63,0,0,0,81.6,416H430.4A33.69,33.69,0,0,0,464,382.3V180.2A33.27,33.27,0,0,0,430.4,147ZM256,365.5c-50.9,0-92.4-41.6-92.4-92.6s41.5-92.6,92.4-92.6a92.55,92.55,0,0,1,92.4,92.6C348.4,323.9,307,365.5,256,365.5Zm168.1-165a14.1,14.1,0,1,1,14-14.1A14.06,14.06,0,0,1,424.1,200.5Z" transform="translate(-48 -96)"/>
                                    <path d="M256,202.9a70,70,0,1,0,69.8,70A69.84,69.84,0,0,0,256,202.9Z" transform="translate(-48 -96)"/>
                                </g>
                            </svg>
                        </div>
                        <div class="photo-edit__text">Изменить фон</div>
                    </div>
                </label>
            </div>
            <div class="container header-container">
                <div class="header-inner__header">
                    <div class="header-inner__user">
                        <div style="background-image: url('{{$user->photo or ''}}')" class="user-avatar"></div>
                        <div class="header-inner__user-name">{{$user->name or ''}}</div>
                    </div>
                </div>
                <div class="user">
                    <div class="user__info">
                        <div class="user__name">
                            <input name="name" type="text" value="{{$album->name or ''}}" placeholder="" class="user-edit__input">
                        </div>
                        <div class="user__desc">
                            <textarea name="description"
                                    class="user-edit__input user-edit__textarea">{{$album->description or ''}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <script type="text/x-handlebars-template" id="header-alerts-template">
        @{{#each messages}}
        <li class="header__alerts-item">
            @{{ text }}
        </li>
        @{{/each }}
    </script>
@endsection
@section('content')
<div class="page-content">
        <section class="section albums albums_inner">
            <div class="container">
                @if(Auth::id() == $album->user_id)
                <div class="albums__add-btn">
                    <a href="#fileupload" class="edit-btn add-photos edit-btn_add">
                        <div class="edit-btn__inner">
                            <div class="edit-btn__icon">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="20px" height="20px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve">
                            <path d="M12.448,17.552C12.448,18.904,11.352,20,10,20l0,0c-1.352,0-2.447-1.096-2.447-2.448V2.448
                                C7.553,1.096,8.648,0,10,0l0,0c1.352,0,2.448,1.096,2.448,2.448V17.552z"/>
                                    <path d="M17.553,7.552C18.904,7.552,20,8.648,20,10l0,0c0,1.351-1.096,2.448-2.447,2.448H2.449
                                C1.096,12.448,0,11.352,0,10l0,0c0-1.353,1.096-2.449,2.449-2.449H17.553z"/>
                        </svg>
                            </div>
                            <div class="edit-btn__text">Добавить</div>
                        </div>
                    </a>
                </div>

                @endif
                <ul class="projects projects_albums">
                    @foreach($photos as $photo)
                    <li @if($album->background_id == $photo->id)id="album-cover"@endif
                            class="projects__item projects__item_inner"

                        data-id="{{$photo->id}}">
                        <a href="#pic-view" data-photo-id="{{$photo->id}}" data-id="{{$album->id}}"
                           style="background-image:url('{{$photo->thumb or ''}}')" class="projects__pic show-album">
                            <div class="project-activity">
                                <div class="project-activity__elem">
                                    <div class="project-activity__icon">
                                        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 17.21">
                                            <path class="cls-1" d="M256.15,247.4c-5.52-.11-10.07,3.29-10.15,7.6a6.55,6.55,0,0,0,1.4,4.11h0c1.57,2-.14,5.49-0.14,5.49l5.07-2.18a12.53,12.53,0,0,0,3.52.57c5.52,0.11,10.07-3.29,10.15-7.6s-4.32-7.89-9.85-8m-4.43,9A1.23,1.23,0,1,1,253,255.2a1.23,1.23,0,0,1-1.23,1.23m4.11,0a1.23,1.23,0,1,1,1.23-1.23,1.23,1.23,0,0,1-1.23,1.23m4.11,0a1.23,1.23,0,1,1,1.23-1.23,1.23,1.23,0,0,1-1.23,1.23" transform="translate(-246 -247.39)"/>
                                        </svg>
                                    </div>
                                    <div class="project-activity__counter">{{$photo->Comments_count or '0'}}</div>
                                </div>
                                <div class="project-activity__elem">
                                    <div class="project-activity__icon">
                                        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 16.86">
                                            <path class="cls-1" d="M74,60.86a5.28,5.28,0,0,0-10-2.39,5.28,5.28,0,0,0-10,2.39c0,5.52,8.87,11.57,10,11.57S74,66.41,74,60.86Z" transform="translate(-54 -55.57)"/>
                                        </svg>
                                    </div>
                                    <div class="project-activity__counter">{{$photo->Likes_count or '0'}}</div>
                                </div>
                            </div></a>
                        <div class="projects__desc">
                            <div class="project-extra">
                                @if(Auth::id() == $album->user_id)
                                <a href="#photo-edit" data-params="description={{$photo->description}}&name={{$photo->name}}&id={{$photo->id}}" class="project-extra__icon show-edit">
                                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 17">
                                        <title>pen</title>
                                        <path d="M21.63,6.37a11.55,11.55,0,0,0-4-4L19,1a4.94,4.94,0,0,1,3,1,4.94,4.94,0,0,1,1,3ZM10,18H6V14l0.48-.48a8.38,8.38,0,0,1,4,4ZM20.48,7.52l-8.85,8.85a11.61,11.61,0,0,0-1.75-2.23,11.57,11.57,0,0,0-2.27-1.75l8.86-8.86A8.37,8.37,0,0,1,20.48,7.52Z" transform="translate(-6 -1)"/>
                                    </svg>
                                </a>
                                @endif
                                <div class="project-extra__desc">
                                    <a>
                                        {{$photo->name}}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </section>
    </div>

<div id="add-album" class="popup form-container">
    <div class="popup__overlay"></div>
    <div class="popup__content">
        <div class="form-container__title">
            <div class="form-container__title-text">Добавить альбом</div><a href="#" class="popup__close"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                                                               width="26.392px" height="26.015px" viewBox="0 0 26.392 26.015" enable-background="new 0 0 26.392 26.015" xml:space="preserve">
	<path fill-rule="evenodd" clip-rule="evenodd" d="M25.896,21.943l-8.963-8.949l8.951-8.938
		c0.787-0.786,0.628-2.219-0.355-3.201c-0.984-0.982-2.42-1.142-3.206-0.356l-9.181,9.167L4.039,0.558
		C3.257-0.223,1.833-0.064,0.858,0.912c-0.977,0.977-1.135,2.401-0.354,3.182l8.881,8.884l-8.893,8.896
		c-0.779,0.779-0.616,2.207,0.364,3.189c0.981,0.98,2.409,1.145,3.188,0.363l9.081-9.083l9.188,9.175
		c0.786,0.784,2.225,0.62,3.214-0.367C26.517,24.165,26.682,22.728,25.896,21.943z"/>
</svg>
            </a>
        </div>
        <form class="store_album">
            {{csrf_field()}}
            <div class="form-container__content">
                <label class="form-container__row">
                    <div class="form-container__left">
                        <div class="form-container__field-name">Название</div>
                    </div>
                    <div class="form-container__right">
                        <input class="input-text input-text_bordered form-container__field" name="name">
                    </div>
                </label>
                <label class="form-container__row">
                    <div class="form-container__left">
                        <div class="form-container__field-name">Описание</div>
                    </div>
                    <div class="form-container__right">
                        <textarea class="input-text input-text_bordered form-container__field" name="description"></textarea>
                    </div>
                </label>
                <div class="form-container__row">
                    <div class="form-container__left">
                        <div style="background-image: url('{{$user->photo or '/img/content/user.jpg'}}')" class="user-avatar"></div>
                    </div>
                    <div class="form-container__right">
                        <label class="file-input">
                            <div class="round-btn round-btn_bordered">Загрузить обложку</div>
                            <input name="cover" type="file" placeholder="" class="file-input__elem">
                        </label>
                        <div class="form-container__desc">
                            (файл должен быть размером
                            не более 1024 КБ)
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-container__btns">
                <input name="" type="button" value="Сохранить" class="round-btn round-btn_filled store_album_btn">
                <a href="#" class="round-btn round-btn_blank">Отменить</a>
                <div class="round-btn round-btn_red form-container__remove">Удалить</div>
            </div>
        </form>
    </div>
</div>

<!--/--------- fileupload ---------\-->
<div id="fileupload" class="popup form-container">
    <div class="popup__overlay"></div>
    <div class="popup__content">
        <div class="form-container__title">
            <div class="form-container__title-text">Добавить фотографии</div>
            <a href="#" class="popup__close"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                                                                   width="26.392px" height="26.015px" viewBox="0 0 26.392 26.015" enable-background="new 0 0 26.392 26.015" xml:space="preserve">
	<path fill-rule="evenodd" clip-rule="evenodd" d="M25.896,21.943l-8.963-8.949l8.951-8.938
		c0.787-0.786,0.628-2.219-0.355-3.201c-0.984-0.982-2.42-1.142-3.206-0.356l-9.181,9.167L4.039,0.558
		C3.257-0.223,1.833-0.064,0.858,0.912c-0.977,0.977-1.135,2.401-0.354,3.182l8.881,8.884l-8.893,8.896
		c-0.779,0.779-0.616,2.207,0.364,3.189c0.981,0.98,2.409,1.145,3.188,0.363l9.081-9.083l9.188,9.175
		c0.786,0.784,2.225,0.62,3.214-0.367C26.517,24.165,26.682,22.728,25.896,21.943z"/>
</svg>
            </a>
        </div>
        <div class="fileupload__wrapper">
            <div class="form-container__content">
                <div class="fileupload">
                    <div class="fileupload__title">
                        <span>Альбом</span> {{ $album->name }}
                    </div>
                    <div class="fileupload__widget-container">
                        <form action="" id="upload-form" enctype="multipart/form-data">
                            <label data-valid-type="file" class="fileupload__widget">
                                <input type="hidden" name="" id="album-id" value="{{$album->id}}">
                                <input type="file" name="cover" id="dropzone" multiple>
                                <ul class="fileupload__photos" id="fileupload-photos"></ul>
                                <div class="fileupload__desc" id="upload-desc">
                                    <div class="photo-edit__link">
                                        <div class="photo-edit__decor">
                                            <div class="photo-edit__icon">
                                                <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 416 320">
                                                    <g>
                                                        <path d="M430.4,147H362.9l-40.4-40.8a0.77,0.77,0,0,0-.3-0.2l-0.2-.2h0A32.71,32.71,0,0,0,298.7,96h-84a33.07,33.07,0,0,0-24.6,10.9V107l-39.5,40h-69A33.21,33.21,0,0,0,48,180.2V382.3A33.63,33.63,0,0,0,81.6,416H430.4A33.69,33.69,0,0,0,464,382.3V180.2A33.27,33.27,0,0,0,430.4,147ZM256,365.5c-50.9,0-92.4-41.6-92.4-92.6s41.5-92.6,92.4-92.6a92.55,92.55,0,0,1,92.4,92.6C348.4,323.9,307,365.5,256,365.5Zm168.1-165a14.1,14.1,0,1,1,14-14.1A14.06,14.06,0,0,1,424.1,200.5Z" transform="translate(-48 -96)"/>
                                                        <path d="M256,202.9a70,70,0,1,0,69.8,70A69.84,69.84,0,0,0,256,202.9Z" transform="translate(-48 -96)"/>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="photo-edit__text">
                                                Перетащите фото сюда </br>
                                                или <span>выберите файлы</span> <br>
                                                размером не более 2мб
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </label>
                        </form>
                    </div>
                    <div class="fileupload__title" id="unload-alert" style="display: none">
                        <span>Следующие файлы не будут загружены</span> (размер более 2мб)
                    </div>
                    <ul class="fileupload__photos" id="unloaded-photos"></ul>
                    <div class="fileuploads__errors">
                        <div class="popup-errors">
                            <ul></ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-container__btns">
                <button name="" type="submit" id="add-photos-btn" class="round-btn round-btn_filled">Сохранить</button>
                <a href="#" class="round-btn close-trigger round-btn_blank">Отменить</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('popups')
    @include('popups')
    @include('handlebars')
@endsection

@section('title')
    Фото Альбом
@endsection

@section('footer')
    @include('parts.footer', ['background' => $album->photo->url])
@endsection