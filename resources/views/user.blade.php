@extends('layouts.internalpage')
@section('content')
    <div class="page-content">
        <section class="section albums">
            <div class="container">
                <div class="section__title">Альбомы пользователя {{$user->name}}</div>
                <ul class="projects projects_albums">
                    @foreach($myalbums as $album)
                        <li class="projects__item">
                            <a href="/album/{{$album->id}}" style="background-image:url('{{$album->photo->thumb or '/img/content/project.jpg'}}')" class="projects__pic show-page">
                                <div class="project__pic-desc">
                                    <div class="project__pic-desc-text">
                                        {{$album->photo->name or ''}}
                                    </div>
                                    <div class="project__pic-desc-photos">{{$album->Photo_Count or '0'}} фотографий</div>
                                </div>
                            </a>
                            <div class="projects__desc">
                                <div class="project-extra">
                                    @if(Auth::id() == $album->user_id)
                                    {{--<a href="#photo-edit" class="project-extra__icon show-edit"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 17">--}}
                                            {{--<title>pen</title>--}}
                                            {{--<path d="M21.63,6.37a11.55,11.55,0,0,0-4-4L19,1a4.94,4.94,0,0,1,3,1,4.94,4.94,0,0,1,1,3ZM10,18H6V14l0.48-.48a8.38,8.38,0,0,1,4,4ZM20.48,7.52l-8.85,8.85a11.61,11.61,0,0,0-1.75-2.23,11.57,11.57,0,0,0-2.27-1.75l8.86-8.86A8.37,8.37,0,0,1,20.48,7.52Z" transform="translate(-6 -1)"/>--}}
                                        {{--</svg>--}}
                                    {{--</a>--}}
                                    @endif
                                    <div class="project-extra__desc">{{$album->name}}</div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </section>
    </div>
@endsection
@section('title')
    Фото Альбом - {{$user->name}}
@endsection

@section('header_classes')
    header_search
@endsection