@extends('layouts.main')
@section('header')
<header style="background-image:url('{{$user->background or '/img/bg/headers/1.jpg'}}')" class="header @yield('header_classes')">
    <div class="header__alerts" id="header-alerts">
        <ul class="header__alerts-content"></ul>
    </div>
    <div class="header__content">
        <div class="container header-container">
            @if($user->id == $auth_id)
                <a href="#" class="edit-btn header__edit-btn" @if($url != 'home') style="top: 100px;" @endif>
                    <div class="edit-btn__inner">
                        <div class="edit-btn__icon"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 17">
                                <title>pen</title>
                                <path d="M21.63,6.37a11.55,11.55,0,0,0-4-4L19,1a4.94,4.94,0,0,1,3,1,4.94,4.94,0,0,1,1,3ZM10,18H6V14l0.48-.48a8.38,8.38,0,0,1,4,4ZM20.48,7.52l-8.85,8.85a11.61,11.61,0,0,0-1.75-2.23,11.57,11.57,0,0,0-2.27-1.75l8.86-8.86A8.37,8.37,0,0,1,20.48,7.52Z" transform="translate(-6 -1)"/>
                            </svg>

                        </div>
                        <div class="edit-btn__text">Редактировать</div>
                    </div>
                </a>
                <a href="/logout" class="edit-btn header__logout">
                    <div class="edit-btn__inner">
                        <div class="edit-btn__icon">
                            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.89 15">
                                <g>
                                    <path d="M13.16,7.32a5.88,5.88,0,0,0-.4-1.83,5.55,5.55,0,0,0-1.69-2.33,0.88,0.88,0,0,1-.36-1.07A0.83,0.83,0,0,1,12,1.67a7.55,7.55,0,0,1,1,1,6.87,6.87,0,0,1,1.54,2.73,7.48,7.48,0,0,1-1.33,6.87,6.78,6.78,0,0,1-2.6,2A9.17,9.17,0,0,1,9,14.83,6.63,6.63,0,0,1,6.78,15a8.2,8.2,0,0,1-2.13-.52,7.38,7.38,0,0,1-3.42-2.8,7,7,0,0,1-1-2.15A6.64,6.64,0,0,1,0,7,11,11,0,0,1,.25,5.63,7.25,7.25,0,0,1,1,3.79a7.86,7.86,0,0,1,1.72-2,0.88,0.88,0,0,1,1.39.17,0.91,0.91,0,0,1-.31,1.23A5.33,5.33,0,0,0,2.19,5.36a5.42,5.42,0,0,0-.35,3.2A5.63,5.63,0,0,0,5,12.68a5.31,5.31,0,0,0,2.67.57,5.65,5.65,0,0,0,4.19-2.06,5.47,5.47,0,0,0,.88-1.44A6,6,0,0,0,13.16,7.32Z" transform="translate(0 0)"/>
                                    <path d="M6.29,4.06c0-1,0-1.9,0-2.85a1.17,1.17,0,1,1,2.33,0c0,0.42,0,.85,0,1.27q0,2.23,0,4.46A1.15,1.15,0,0,1,8,8a1.14,1.14,0,0,1-1.59-.67A1.92,1.92,0,0,1,6.29,6.8c0-.91,0-1.83,0-2.74h0Z" transform="translate(0 0)"/>
                                </g>
                            </svg>
                        </div>
                        <div class="edit-btn__text">Выход</div>
                    </div>
                </a>
            @endif
            @if($url != 'home')
                <a href="/" class="edit-btn header__go-home">
                    <div class="edit-btn__inner">
                        <div class="edit-btn__icon">
                            <svg version="1.1" id="Home" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 width="20px" height="21.696px" viewBox="59.499 58.652 20 21.696" enable-background="new 59.499 58.652 20 21.696"
                                 xml:space="preserve">
                        <path d="M79.116,66.476l-9.058-7.619c-0.326-0.274-0.801-0.274-1.127,0l-9.121,7.673
                        c-0.282,0.237-0.385,0.625-0.259,0.97c0.126,0.346,0.455,0.576,0.823,0.576h1.331v10.171c0,1.16,0.941,2.101,2.101,2.101h2.976
                        v-5.952c0-0.386,0.313-0.7,0.7-0.7h4.026c0.387,0,0.7,0.314,0.7,0.7v5.952h2.977c1.16,0,2.101-0.94,2.101-2.101V68.076h1.33
                        c0.003,0,0.006,0,0.008,0c0.483,0,0.875-0.392,0.875-0.875C79.499,66.899,79.347,66.633,79.116,66.476z"/>
                    </svg>
                        </div>
                        <div class="edit-btn__text">На главную</div>
                    </div>
                </a>
            @endif
            <div class="user">
                <div class="user__pic">
                    <div id="user-pic" style="background-image: url('{{$user->photo or '/img/no_photo.jpg'}}')" class="user__pic-avatar"></div>
                </div>
                <div class="user__info">
                    <div id="user-name" class="user__name user__name_value">{{$user->name or ''}}</div>
                    <div class="user__desc user__desc_value">
                        {{$user->description or 'Заполните описание и социальные сети нажав на кнопку "Редактировать"'}}
                    </div>
                    <div class="socials">
                        <ul class="socials__list">
                            <li class="socials__item"><a href="{{$user->vk or ''}}"  target="_blank" class="socials__link user__vk">
                                    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                        <g>
                                            <path d="M314.55,159.38a1.43,1.43,0,0,0,.53-0.14,0.8,0.8,0,0,0,.41-0.39,1.33,1.33,0,0,0,.11-0.58h0a1.14,1.14,0,0,0-.12-0.5,0.74,0.74,0,0,0-.39-0.37,1.54,1.54,0,0,0-.59-0.12h-1.19v2.12h0.32Z" transform="translate(-304.5 -150.44)"/>
                                            <path d="M315.5,161.22a1.85,1.85,0,0,0-.63-0.1h-1.54v2.5h1.49a2.16,2.16,0,0,0,.77-0.18,1,1,0,0,0,.5-0.43,1.29,1.29,0,0,0,.16-0.64,1.21,1.21,0,0,0-.19-0.73A1.12,1.12,0,0,0,315.5,161.22Z" transform="translate(-304.5 -150.44)"/>
                                            <path d="M319.5,150.44h-10a5,5,0,0,0-5,5v10a5,5,0,0,0,5,5h10a5,5,0,0,0,5-5v-10A5,5,0,0,0,319.5,150.44Zm-0.95,13.25a2.69,2.69,0,0,1-.76.93,3.6,3.6,0,0,1-1.24.63,6.44,6.44,0,0,1-1.72.19h-4v-10h3.53a11.3,11.3,0,0,1,1.67.09,3.21,3.21,0,0,1,1.07.35,2,2,0,0,1,.8.77,2.27,2.27,0,0,1,.27,1.12,2.34,2.34,0,0,1-.37,1.3,2.15,2.15,0,0,1-1,.84V160a2.71,2.71,0,0,1,1.48.79,2.28,2.28,0,0,1,.56,1.62h0A2.88,2.88,0,0,1,318.56,163.69Z" transform="translate(-304.5 -150.44)"/>
                                        </g>
                                    </svg>
                                </a></li>
                            <li class="socials__item"><a href="{{$user->facebook or ''}}"  target="_blank" class="socials__link user__facebook">
                                    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                        <path class="cls-1" d="M345.49,150.44h-10a5,5,0,0,0-5,5v10a5,5,0,0,0,5,5h10a5,5,0,0,0,5-5v-10A5,5,0,0,0,345.49,150.44Zm-1.89,6.15h-1.13s-0.8-.16-0.8,1v1h1.93v2.57h-1.93v5.78H339.1v-5.78h-1.93v-2.57h1.77v-2.25a2.39,2.39,0,0,1,2.41-2.25h2.25v2.57Z" transform="translate(-330.49 -150.44)"/>
                                    </svg>
                                </a></li>
                            <li class="socials__item"><a href="{{$user->twitter or ''}}"  target="_blank" class="socials__link user__twitter">
                                    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                        <path class="cls-1" d="M374.69,150.77h-10a5,5,0,0,0-5,5v10a5,5,0,0,0,5,5h10a5,5,0,0,0,5-5v-10A5,5,0,0,0,374.69,150.77Zm1.37,6.38a2,2,0,0,1-.72,1,7.45,7.45,0,0,0-.72.58,5.42,5.42,0,0,1-2,5.16,7.79,7.79,0,0,1-6.88,1.43c-0.29-.14-1.29-0.72-1.29-0.72a16.49,16.49,0,0,0,2.29-.58,3.1,3.1,0,0,0,1-.58,5.21,5.21,0,0,1-1.57-.43,2.73,2.73,0,0,1-.72-1.15h1.15a4.24,4.24,0,0,1-1.29-1,2.74,2.74,0,0,1-.43-1.43l0.72,0.14a2.65,2.65,0,0,1-.72-2,3.1,3.1,0,0,1,.29-1.29,4.46,4.46,0,0,0,2.87,2.29c1.87,0.29,2,.29,2,0.29-0.61-4,4.48-3,4.16-2a5,5,0,0,0,1.29-.86,7.28,7.28,0,0,1-.29,1.29l0.86-.14h0Z" transform="translate(-359.69 -150.77)"/>
                                    </svg>
                                </a></li>
                            <li class="socials__item"><a href="{{$user->googleplus or ''}}" target="_blank" class="socials__link user__googleplus">
                                    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                        <g>
                                            <path class="cls-1" d="M393.07,155.93c-2.13.39-1,4.48,1.11,4S394.95,155.58,393.07,155.93Z" transform="translate(-384.85 -150.77)"/>
                                            <path class="cls-1" d="M394.59,162.4a1.49,1.49,0,0,0-.93-0.07h0c-1.24.1-2.49,0.45-2.5,1.74,0,1.53,2.92,2.15,4.24,1.31a1.38,1.38,0,0,0,.71-1.37A3.33,3.33,0,0,0,394.59,162.4Z" transform="translate(-384.85 -150.77)"/>
                                            <path class="cls-1" d="M399.85,150.77h-10a5,5,0,0,0-5,5v10a5,5,0,0,0,5,5h10a5,5,0,0,0,5-5v-10A5,5,0,0,0,399.85,150.77ZM397,164.52c-1.14,2.37-5.7,2.34-6.83.78a1.67,1.67,0,0,1-.24-1.56c0.52-1.47,2.58-1.8,4-1.87a2.92,2.92,0,0,0-.22-0.31,1.11,1.11,0,0,1-.08-1.21c-3.61.17-3.37-3-2.46-3.93a2.65,2.65,0,0,1,1.12-.75,5.21,5.21,0,0,1,1.54-.33c0.41,0,3.45-.06,3.71,0-0.09,0-.27.17-0.37,0.23a1.16,1.16,0,0,1-.87.38,5,5,0,0,0-.61,0,2.38,2.38,0,0,1,.81,2.74c-0.46,1-2,1.44-1.45,2.25C395.39,161.45,397.94,162.47,397,164.52Zm4.48-3.65H399.7v1.64h-0.58l0-1.65h-1.78V160.3h1.79v-1.66h0.6a12.79,12.79,0,0,0,0,1.66h1.74v0.57Z" transform="translate(-384.85 -150.77)"/>
                                        </g>
                                    </svg>
                                </a></li>
                            <li class="socials__item"><a href="mailto:{{$user->email or ''}}" class="socials__link user__email">
                                    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                        <path class="cls-1" d="M422.85,150.77h-10a5,5,0,0,0-5,5v10a5,5,0,0,0,5,5h10a5,5,0,0,0,5-5v-10A5,5,0,0,0,422.85,150.77ZM412.46,158l3.68,2.81-3.68,3.68V158Zm0.36,6.85,3.72-3.72,1.5,1.15,1.5-1.15,3.72,3.72H412.82Zm10.81-.36L420,160.8l3.67-2.81v6.49Zm0-7.13L418,161.63l-5.58-4.28v-0.64h11.17v0.64Z" transform="translate(-407.85 -150.77)"/>
                                    </svg>
                                </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header__bar">
        <div class="container">
            <div class="search">
                <form class="search__form" action="/search" method="post">
                    {{csrf_field()}}
                    <input name="s" type="text" placeholder="Исследовать мир" class="search__input input-text">
                    <input name="" type="submit" value="" class="search__submit">
                </form>
            </div>
        </div>
        <div class="edit-bar">
            <div class="container">
                <a href="#" class="edit-bar__btn edit-bar__btn_cancel round-btn round-btn_blank">Отменить</a>
                <a href="#" class="edit-bar__btn round-btn round-btn_filled saveUserData">Сохранить</a></div>
        </div>
    </div>
    <div class="user-edit">
        <form class="edit-header-form" method="POST" action="/user/saveData">
            {{csrf_field()}}
            <div class="container header-container">
                <div class="photo-edit photo-edit_static">
                    <label class="photo-edit__link">
                        <input name="background" type="file" value="" class="photo-edit__file">
                        <div class="photo-edit__decor">
                            <div class="photo-edit__icon"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 416 320">
                                    <g>
                                        <path d="M430.4,147H362.9l-40.4-40.8a0.77,0.77,0,0,0-.3-0.2l-0.2-.2h0A32.71,32.71,0,0,0,298.7,96h-84a33.07,33.07,0,0,0-24.6,10.9V107l-39.5,40h-69A33.21,33.21,0,0,0,48,180.2V382.3A33.63,33.63,0,0,0,81.6,416H430.4A33.69,33.69,0,0,0,464,382.3V180.2A33.27,33.27,0,0,0,430.4,147ZM256,365.5c-50.9,0-92.4-41.6-92.4-92.6s41.5-92.6,92.4-92.6a92.55,92.55,0,0,1,92.4,92.6C348.4,323.9,307,365.5,256,365.5Zm168.1-165a14.1,14.1,0,1,1,14-14.1A14.06,14.06,0,0,1,424.1,200.5Z" transform="translate(-48 -96)"/>
                                        <path d="M256,202.9a70,70,0,1,0,69.8,70A69.84,69.84,0,0,0,256,202.9Z" transform="translate(-48 -96)"/>
                                    </g>
                                </svg>

                            </div>
                            <div class="photo-edit__text">Изменить фон</div>
                        </div>
                    </label>
                </div>
                <div class="user">
                    <div class="user__pic">
                        <label style="background-image: url('{{$user->photo or '/img/content/user.jpg'}}')" class="user__pic-avatar photo-edit photo-edit_attached">
                            <input name="photo" type="file" class="photo-edit__input-file">
                            <div class="photo-edit__link">
                                <div class="photo-edit__decor">
                                    <div class="photo-edit__icon"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 416 320">
                                            <g>
                                                <path d="M430.4,147H362.9l-40.4-40.8a0.77,0.77,0,0,0-.3-0.2l-0.2-.2h0A32.71,32.71,0,0,0,298.7,96h-84a33.07,33.07,0,0,0-24.6,10.9V107l-39.5,40h-69A33.21,33.21,0,0,0,48,180.2V382.3A33.63,33.63,0,0,0,81.6,416H430.4A33.69,33.69,0,0,0,464,382.3V180.2A33.27,33.27,0,0,0,430.4,147ZM256,365.5c-50.9,0-92.4-41.6-92.4-92.6s41.5-92.6,92.4-92.6a92.55,92.55,0,0,1,92.4,92.6C348.4,323.9,307,365.5,256,365.5Zm168.1-165a14.1,14.1,0,1,1,14-14.1A14.06,14.06,0,0,1,424.1,200.5Z" transform="translate(-48 -96)"/>
                                                <path d="M256,202.9a70,70,0,1,0,69.8,70A69.84,69.84,0,0,0,256,202.9Z" transform="translate(-48 -96)"/>
                                            </g>
                                        </svg>

                                    </div>
                                    <div class="photo-edit__text">Изменить фото</div>
                                </div>
                            </div>
                        </label>
                    </div>
                    <div class="user__info">
                        <div class="user__name">
                            <input name="name" type="text" value="{{$user->name or ''}}" placeholder="" class="user-edit__input">
                        </div>
                        <div class="user__desc">
                            <textarea class="user-edit__input user-edit__textarea" name="description">{{$user->description or ''}}</textarea>
                        </div>
                        <div class="socials">
                            <ul class="socials__list">
                                <li class="socials__item">
                                    <div class="socials__link"><a href="#" class="socials__icon-wrapper"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                <g>
                                                    <path d="M314.55,159.38a1.43,1.43,0,0,0,.53-0.14,0.8,0.8,0,0,0,.41-0.39,1.33,1.33,0,0,0,.11-0.58h0a1.14,1.14,0,0,0-.12-0.5,0.74,0.74,0,0,0-.39-0.37,1.54,1.54,0,0,0-.59-0.12h-1.19v2.12h0.32Z" transform="translate(-304.5 -150.44)"/>
                                                    <path d="M315.5,161.22a1.85,1.85,0,0,0-.63-0.1h-1.54v2.5h1.49a2.16,2.16,0,0,0,.77-0.18,1,1,0,0,0,.5-0.43,1.29,1.29,0,0,0,.16-0.64,1.21,1.21,0,0,0-.19-0.73A1.12,1.12,0,0,0,315.5,161.22Z" transform="translate(-304.5 -150.44)"/>
                                                    <path d="M319.5,150.44h-10a5,5,0,0,0-5,5v10a5,5,0,0,0,5,5h10a5,5,0,0,0,5-5v-10A5,5,0,0,0,319.5,150.44Zm-0.95,13.25a2.69,2.69,0,0,1-.76.93,3.6,3.6,0,0,1-1.24.63,6.44,6.44,0,0,1-1.72.19h-4v-10h3.53a11.3,11.3,0,0,1,1.67.09,3.21,3.21,0,0,1,1.07.35,2,2,0,0,1,.8.77,2.27,2.27,0,0,1,.27,1.12,2.34,2.34,0,0,1-.37,1.3,2.15,2.15,0,0,1-1,.84V160a2.71,2.71,0,0,1,1.48.79,2.28,2.28,0,0,1,.56,1.62h0A2.88,2.88,0,0,1,318.56,163.69Z" transform="translate(-304.5 -150.44)"/>
                                                </g>
                                            </svg>
                                        </a>
                                        <div class="socials__link-dropdown">
                                            <div class="socials__edit-form">
                                                <input name="" type="hidden" class="socials__link-hidden">
                                                <input name="vk" value="{{$user->vk or ''}}" type="text" placeholder="" class="socials__edit-input input-text input-text_bordered">
                                                <div class="socials__edit-btns"><a href="#" class="socials__edit-btn round-btn socials-save">Сохранить</a><a href="#" class="socials__edit-btn round-btn round-btn_blank socials-cancel">Отменить</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="socials__item">
                                    <div class="socials__link"><a href="#" class="socials__icon-wrapper"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                <path class="cls-1" d="M345.49,150.44h-10a5,5,0,0,0-5,5v10a5,5,0,0,0,5,5h10a5,5,0,0,0,5-5v-10A5,5,0,0,0,345.49,150.44Zm-1.89,6.15h-1.13s-0.8-.16-0.8,1v1h1.93v2.57h-1.93v5.78H339.1v-5.78h-1.93v-2.57h1.77v-2.25a2.39,2.39,0,0,1,2.41-2.25h2.25v2.57Z" transform="translate(-330.49 -150.44)"/>
                                            </svg>
                                        </a>
                                        <div class="socials__link-dropdown">
                                            <div class="socials__edit-form">
                                                <input name="" type="hidden" class="socials__link-hidden">
                                                <input name="facebook" value="{{$user->facebook or ''}}" type="text" placeholder="" class="socials__edit-input input-text input-text_bordered">
                                                <div class="socials__edit-btns"><a href="#" class="socials__edit-btn round-btn socials-save">Сохранить</a><a href="#" class="socials__edit-btn round-btn round-btn_blank socials-cancel">Отменить</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="socials__item">
                                    <div class="socials__link"><a href="#" class="socials__icon-wrapper"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                <path class="cls-1" d="M374.69,150.77h-10a5,5,0,0,0-5,5v10a5,5,0,0,0,5,5h10a5,5,0,0,0,5-5v-10A5,5,0,0,0,374.69,150.77Zm1.37,6.38a2,2,0,0,1-.72,1,7.45,7.45,0,0,0-.72.58,5.42,5.42,0,0,1-2,5.16,7.79,7.79,0,0,1-6.88,1.43c-0.29-.14-1.29-0.72-1.29-0.72a16.49,16.49,0,0,0,2.29-.58,3.1,3.1,0,0,0,1-.58,5.21,5.21,0,0,1-1.57-.43,2.73,2.73,0,0,1-.72-1.15h1.15a4.24,4.24,0,0,1-1.29-1,2.74,2.74,0,0,1-.43-1.43l0.72,0.14a2.65,2.65,0,0,1-.72-2,3.1,3.1,0,0,1,.29-1.29,4.46,4.46,0,0,0,2.87,2.29c1.87,0.29,2,.29,2,0.29-0.61-4,4.48-3,4.16-2a5,5,0,0,0,1.29-.86,7.28,7.28,0,0,1-.29,1.29l0.86-.14h0Z" transform="translate(-359.69 -150.77)"/>
                                            </svg>
                                        </a>
                                        <div class="socials__link-dropdown">
                                            <div class="socials__edit-form">
                                                <input name="" type="hidden" class="socials__link-hidden">
                                                <input name="twitter" value="{{$user->twitter or ''}}" type="text" placeholder="" class="socials__edit-input input-text input-text_bordered">
                                                <div class="socials__edit-btns"><a href="#" class="socials__edit-btn round-btn socials-save">Сохранить</a><a href="#" class="socials__edit-btn round-btn round-btn_blank socials-cancel">Отменить</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="socials__item">
                                    <div class="socials__link"><a href="#" class="socials__icon-wrapper"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                <g>
                                                    <path class="cls-1" d="M393.07,155.93c-2.13.39-1,4.48,1.11,4S394.95,155.58,393.07,155.93Z" transform="translate(-384.85 -150.77)"/>
                                                    <path class="cls-1" d="M394.59,162.4a1.49,1.49,0,0,0-.93-0.07h0c-1.24.1-2.49,0.45-2.5,1.74,0,1.53,2.92,2.15,4.24,1.31a1.38,1.38,0,0,0,.71-1.37A3.33,3.33,0,0,0,394.59,162.4Z" transform="translate(-384.85 -150.77)"/>
                                                    <path class="cls-1" d="M399.85,150.77h-10a5,5,0,0,0-5,5v10a5,5,0,0,0,5,5h10a5,5,0,0,0,5-5v-10A5,5,0,0,0,399.85,150.77ZM397,164.52c-1.14,2.37-5.7,2.34-6.83.78a1.67,1.67,0,0,1-.24-1.56c0.52-1.47,2.58-1.8,4-1.87a2.92,2.92,0,0,0-.22-0.31,1.11,1.11,0,0,1-.08-1.21c-3.61.17-3.37-3-2.46-3.93a2.65,2.65,0,0,1,1.12-.75,5.21,5.21,0,0,1,1.54-.33c0.41,0,3.45-.06,3.71,0-0.09,0-.27.17-0.37,0.23a1.16,1.16,0,0,1-.87.38,5,5,0,0,0-.61,0,2.38,2.38,0,0,1,.81,2.74c-0.46,1-2,1.44-1.45,2.25C395.39,161.45,397.94,162.47,397,164.52Zm4.48-3.65H399.7v1.64h-0.58l0-1.65h-1.78V160.3h1.79v-1.66h0.6a12.79,12.79,0,0,0,0,1.66h1.74v0.57Z" transform="translate(-384.85 -150.77)"/>
                                                </g>
                                            </svg>
                                        </a>
                                        <div class="socials__link-dropdown">
                                            <div class="socials__edit-form">
                                                <input name="" type="hidden" class="socials__link-hidden">
                                                <input name="googleplus" value="{{$user->googleplus or ''}}" type="text" placeholder="" class="socials__edit-input input-text input-text_bordered">
                                                <div class="socials__edit-btns"><a href="#" class="socials__edit-btn round-btn socials-save">Сохранить</a><a href="#" class="socials__edit-btn round-btn round-btn_blank socials-cancel">Отменить</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="socials__item">
                                    <div class="socials__link"><a href="#" class="socials__icon-wrapper"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                <path class="cls-1" d="M422.85,150.77h-10a5,5,0,0,0-5,5v10a5,5,0,0,0,5,5h10a5,5,0,0,0,5-5v-10A5,5,0,0,0,422.85,150.77ZM412.46,158l3.68,2.81-3.68,3.68V158Zm0.36,6.85,3.72-3.72,1.5,1.15,1.5-1.15,3.72,3.72H412.82Zm10.81-.36L420,160.8l3.67-2.81v6.49Zm0-7.13L418,161.63l-5.58-4.28v-0.64h11.17v0.64Z" transform="translate(-407.85 -150.77)"/>
                                            </svg>
                                        </a>
                                        <div class="socials__link-dropdown">
                                            <div class="socials__edit-form">
                                                <input name="" type="hidden" class="socials__link-hidden">
                                                <input name="email" value="{{$user->email or ''}}" type="text" placeholder="" class="socials__edit-input input-text input-text_bordered">
                                                <div class="socials__edit-btns">
                                                    <a href="javascript: false" class="socials__edit-btn round-btn socials-save">Сохранить</a>
                                                    <a href="#" class="socials__edit-btn round-btn round-btn_blank socials-cancel">Отменить</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</header>

<script type="text/x-handlebars-template" id="header-alerts-template">
    @{{#each messages}}
    <li class="header__alerts-item">
        @{{ text }}
    </li>
    @{{/each }}
</script>
@endsection


@section('footer')
    @include('parts.footer', ['background' => $user->background])
@endsection