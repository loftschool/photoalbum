@extends('layouts.internalpage')
@section('content')
    <div class="page-content">
        <section class="new section">
            @if(!empty($search))
            <div class="container">
                <div class="search-title">По запросу «{{$tag or ''}}{{$search}}» найден(о) {{$photos->count()}} фотографий</div>
                <ul class="projects">
                    @foreach($photos as $result)
                    <li class="projects__item">
                        <a href="/album/{{$result->album->id}}" style="background-image:url('{{$result->url}}')" class="projects__pic show-page"></a>
                        <div class="projects__desc">
                            <div class="project-info">
                                <div class="project-info__pic">
                                    <a href="/user/{{$result->user->id}}">
                                        <div style="background-image:url('{{$result->user->photo or ''}}')" class="project-info__user"></div>
                                    </a>
                                </div>
                                <div class="project-info__desc">
                                    <div class="project-info__title">{{$result->name or 'Без описания'}}</div>
                                    <div class="project-activity">
                                        <a href="#" class="project-activity__elem">
                                            <div class="project-activity__icon"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 17.21">
                                                    <path class="cls-1" d="M256.15,247.4c-5.52-.11-10.07,3.29-10.15,7.6a6.55,6.55,0,0,0,1.4,4.11h0c1.57,2-.14,5.49-0.14,5.49l5.07-2.18a12.53,12.53,0,0,0,3.52.57c5.52,0.11,10.07-3.29,10.15-7.6s-4.32-7.89-9.85-8m-4.43,9A1.23,1.23,0,1,1,253,255.2a1.23,1.23,0,0,1-1.23,1.23m4.11,0a1.23,1.23,0,1,1,1.23-1.23,1.23,1.23,0,0,1-1.23,1.23m4.11,0a1.23,1.23,0,1,1,1.23-1.23,1.23,1.23,0,0,1-1.23,1.23" transform="translate(-246 -247.39)"/>
                                                </svg>
                                            </div>
                                            <div class="project-activity__counter">{{$result->Comments_Count or '0'}}</div></a>
                                        <a href="#" class="project-activity__elem">
                                            <div class="project-activity__icon"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 16.86">
                                                    <path class="cls-1" d="M74,60.86a5.28,5.28,0,0,0-10-2.39,5.28,5.28,0,0,0-10,2.39c0,5.52,8.87,11.57,10,11.57S74,66.41,74,60.86Z" transform="translate(-54 -55.57)"/>
                                                </svg>

                                            </div>
                                            <div class="project-activity__counter">{{$result->Likes_Count or '0'}}</div></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="projects__extra">
                            <div class="project-extra">
                                <div class="project-extra__icon">
                                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 18.18">
                                        <title>folders</title>
                                        <g id="miu">
                                            <g id="Artboard-1">
                                                <path id="editor-album-collection-glyph" d="M2.13,6.55A0.12,0.12,0,0,0,2,6.66V21a0.12,0.12,0,0,0,.13.12H21.87A0.12,0.12,0,0,0,22,21V6.66a0.12,0.12,0,0,0-.13-0.12H2.13ZM3.82,2.91V3.82H20.18V2.91H3.82ZM2.91,4.73V5.64H21.09V4.73H2.91Z" transform="translate(-2 -2.91)"/>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                                <div class="project-extra__desc"><a href="/album/{{$result->album->id}}">{{$result->album->name}}</a></div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>

                @if(!empty($albums))
                <div class="search-title">По запросу «{{$search}}» найден(о) {{$albums->count()}} альбом(ов)</div>
                <ul class="projects">
                    @foreach($albums as $result)
                        <li class="projects__item">
                            <a href="/album/{{$result->id}}" style="background-image:url('{{$result->photo->url}}')" class="projects__pic show-page"></a>
                            <div class="projects__desc">
                                <div class="project-info">
                                    <div class="project-info__pic">
                                        <a href="/user/{{$result->user->id}}">
                                            <div style="background-image:url('{{$result->user->photo or ''}}')" class="project-info__user"></div>
                                        </a>
                                    </div>
                                    <div class="project-info__desc">
                                        <div class="project-info__title">{{$result->name or 'Без описания'}}</div>
                                        <div class="project-activity"><a href="#" class="project-activity__elem">
                                                <div class="project-activity__icon"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 17.21">
                                                        <path class="cls-1" d="M256.15,247.4c-5.52-.11-10.07,3.29-10.15,7.6a6.55,6.55,0,0,0,1.4,4.11h0c1.57,2-.14,5.49-0.14,5.49l5.07-2.18a12.53,12.53,0,0,0,3.52.57c5.52,0.11,10.07-3.29,10.15-7.6s-4.32-7.89-9.85-8m-4.43,9A1.23,1.23,0,1,1,253,255.2a1.23,1.23,0,0,1-1.23,1.23m4.11,0a1.23,1.23,0,1,1,1.23-1.23,1.23,1.23,0,0,1-1.23,1.23m4.11,0a1.23,1.23,0,1,1,1.23-1.23,1.23,1.23,0,0,1-1.23,1.23" transform="translate(-246 -247.39)"/>
                                                    </svg>

                                                </div>
                                                <div class="project-activity__counter">{{$result->photo->Comments_Count or '0'}}</div></a>
                                            <a href="#" class="project-activity__elem">
                                                <div class="project-activity__icon"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 16.86">
                                                        <path class="cls-1" d="M74,60.86a5.28,5.28,0,0,0-10-2.39,5.28,5.28,0,0,0-10,2.39c0,5.52,8.87,11.57,10,11.57S74,66.41,74,60.86Z" transform="translate(-54 -55.57)"/>
                                                    </svg>

                                                </div>
                                                <div class="project-activity__counter">{{$result->photo->Likes_Count or '0'}}</div></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="projects__extra">
                                <div class="project-extra">
                                    <div class="project-extra__icon"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 18.18">
                                            <title>folders</title>
                                            <g id="miu">
                                                <g id="Artboard-1">
                                                    <path id="editor-album-collection-glyph" d="M2.13,6.55A0.12,0.12,0,0,0,2,6.66V21a0.12,0.12,0,0,0,.13.12H21.87A0.12,0.12,0,0,0,22,21V6.66a0.12,0.12,0,0,0-.13-0.12H2.13ZM3.82,2.91V3.82H20.18V2.91H3.82ZM2.91,4.73V5.64H21.09V4.73H2.91Z" transform="translate(-2 -2.91)"/>
                                                </g>
                                            </g>
                                        </svg>

                                    </div>
                                    <div class="project-extra__desc"><a href="/album/{{$result->id}}">{{$result->name}}</a></div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
                @endif
            </div>
            @else
                <h1>Пустой запрос поиска</h1>
            @endif
        </section>
    </div>
@endsection

@section('title')
    Фото Альбом
@endsection

@section('header_classes')
    header_search
@endsection