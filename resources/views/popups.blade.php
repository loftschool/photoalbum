<!--/--------- просмотр альбома ---------\-->
<div id="pic-view" class="popup pic-view">
    <div class="popup__overlay"></div>
    <div class="popup__content">
        <a href="#" class="popup__close">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 width="26.392px" height="26.015px" viewBox="0 0 26.392 26.015"
                 enable-background="new 0 0 26.392 26.015" xml:space="preserve">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M25.896,21.943l-8.963-8.949l8.951-8.938
                    c0.787-0.786,0.628-2.219-0.355-3.201c-0.984-0.982-2.42-1.142-3.206-0.356l-9.181,9.167L4.039,0.558
                    C3.257-0.223,1.833-0.064,0.858,0.912c-0.977,0.977-1.135,2.401-0.354,3.182l8.881,8.884l-8.893,8.896
                    c-0.779,0.779-0.616,2.207,0.364,3.189c0.981,0.98,2.409,1.145,3.188,0.363l9.081-9.083l9.188,9.175
                    c0.786,0.784,2.225,0.62,3.214-0.367C26.517,24.165,26.682,22.728,25.896,21.943z"/>
            </svg>
        </a>
        <div class="pic-view__container">

        </div>
    </div>
</div>


<!--/--------- добавление альбома ---------\-->
<div id="add-album" class="popup form-container">
    <div class="popup__overlay"></div>
    <div class="popup__content">
        <div class="form-container__title">
            <div class="form-container__title-text">Добавить альбом</div>
            <a href="#" class="popup__close">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="26.392px" height="26.015px" viewBox="0 0 26.392 26.015"
                     enable-background="new 0 0 26.392 26.015" xml:space="preserve">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M25.896,21.943l-8.963-8.949l8.951-8.938
                        c0.787-0.786,0.628-2.219-0.355-3.201c-0.984-0.982-2.42-1.142-3.206-0.356l-9.181,9.167L4.039,0.558
                        C3.257-0.223,1.833-0.064,0.858,0.912c-0.977,0.977-1.135,2.401-0.354,3.182l8.881,8.884l-8.893,8.896
                        c-0.779,0.779-0.616,2.207,0.364,3.189c0.981,0.98,2.409,1.145,3.188,0.363l9.081-9.083l9.188,9.175
                        c0.786,0.784,2.225,0.62,3.214-0.367C26.517,24.165,26.682,22.728,25.896,21.943z"/>
                </svg>
            </a>
        </div>
        <form class="store_album">
            {{csrf_field()}}
            <div class="form-container__content">
                <label class="form-container__row">
                    <div class="form-container__left">
                        <div class="form-container__field-name">Название</div>
                    </div>
                    <div class="form-container__right">
                        <input data-valid-name="Название" data-valid-type="text" class="input-text input-text_bordered form-container__field" name="name">
                    </div>
                </label>
                <label class="form-container__row">
                    <div class="form-container__left">
                        <div class="form-container__field-name">Описание</div>
                    </div>
                    <div class="form-container__right">
                        <textarea data-valid-name="Описание" data-valid-type="text" class="input-text input-text_bordered form-container__field"
                                  name="description"></textarea>
                    </div>
                </label>
                <div class="form-container__row">
                    <div class="form-container__left">
                        <div style="background-image: url('/img/no_photo.jpg')"
                             class="user-avatar"></div>
                    </div>
                    <div class="form-container__right">
                        <label class="file-input">
                            <div class="round-btn round-btn_bordered error-attach">Загрузить обложку</div>
                            <input data-valid-type="file" name="cover" type="file" placeholder="" class="file-input__elem">
                        </label>
                        <div class="form-container__desc">
                            (файл должен быть размером
                            не более 1024 КБ)
                        </div>
                    </div>
                </div>
                 <div class="form-container__row">
                     <div class="errors-container">
                         <div class="popup-errors">
                             <ul></ul>
                         </div>
                     </div>
                 </div>
            </div>
            <div class="form-container__btns">
                <input name="" type="submit" value="Сохранить" class="round-btn round-btn_filled store_album_btn">
                <a href="#" class="round-btn round-btn_blank">Отменить</a>
            </div>
        </form>
    </div>
</div>

<!--/--------- измененеие альбома / редактирование фотографии ---------\-->
<div id="photo-edit" class="popup photo-edit form-container">
    <div class="popup__overlay"></div>
    <div class="popup__content">
        <div class="form-container__title">
            <div class="form-container__title-text">Отредактировать @if(!empty($myalbums))альбом@elseфотографию@endif</div>
            <a href="#" class="popup__close close-trigger">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="26.392px" height="26.015px" viewBox="0 0 26.392 26.015"
                     enable-background="new 0 0 26.392 26.015" xml:space="preserve">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M25.896,21.943l-8.963-8.949l8.951-8.938
                        c0.787-0.786,0.628-2.219-0.355-3.201c-0.984-0.982-2.42-1.142-3.206-0.356l-9.181,9.167L4.039,0.558
                        C3.257-0.223,1.833-0.064,0.858,0.912c-0.977,0.977-1.135,2.401-0.354,3.182l8.881,8.884l-8.893,8.896
                        c-0.779,0.779-0.616,2.207,0.364,3.189c0.981,0.98,2.409,1.145,3.188,0.363l9.081-9.083l9.188,9.175
                        c0.786,0.784,2.225,0.62,3.214-0.367C26.517,24.165,26.682,22.728,25.896,21.943z"/>
                </svg>
            </a>
        </div>
        <form id="edit-album" action="@if(!empty($myalbums))/albums/update/@else/photos/update/@endif">
            <input type="hidden" name="id" class="form-container__id">
            <div class="form-container__content">
                <label class="form-container__row">
                    <div class="form-container__left">
                        <div class="form-container__field-name">Название</div>
                    </div>
                    <div class="form-container__right">
                        <input data-valid-name="Название" data-valid-type="text" name="name" class="input-text input-text_bordered form-container__field">
                    </div>
                </label>
                <label class="form-container__row">
                    <div class="form-container__left">
                        <div class="form-container__field-name">Описание</div>
                    </div>
                    <div class="form-container__right">
                        <textarea data-valid-name="Описание" data-valid-type="text" name="description" class="input-text input-text_bordered form-container__field"></textarea>
                    </div>
                </label>
                <div class="form-container__row">
                    <div class="errors-container">
                        <div class="popup-errors">
                            <ul></ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-container__btns">
                <input name="" type="submit" value="Сохранить" class="round-btn round-btn_filled">
                <a href="#" class="round-btn close-trigger round-btn_blank">Отменить</a>

                @if(!empty($myalbums))
                    <a class="round-btn round-btn_red form-container__remove" id="remove-album">Удалить</a>
                @else
                    <a class="round-btn round-btn_red form-container__remove" id="remove-photo">Удалить</a>
                @endif
            </div>
        </form>
    </div>
</div>

<!--/--------- всплывашка уведомлений ---------\-->
<div id="alert-popup" class="popup photo-edit form-container">
    <div class="popup__overlay"></div>
    <div class="popup__content">
        <div class="form-container__content">
            <h1 class="alert-message">Тест тест</h1>
        </div>
        <div class="form-container__btns">
            <a href="#" class="round-btn close-trigger">Закрыть</a>
        </div>
    </div>
</div>