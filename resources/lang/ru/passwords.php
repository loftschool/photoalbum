<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен иметь 6 и более символов',
    'reset' => 'Ваш пароль был сброшен',
    'sent' => 'Мы выслали вам e-mail для подтверждения',
    'token' => 'Неправильный токен для сброса пароля',
    'user' => "Мы не можем найти пользователя с таким email",

];
