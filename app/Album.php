<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Album extends Model
{
    protected $hidden = ['Photo_count', 'photos'];
    protected $appends = array('Photo_count');

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function photo()
    {
        return $this->hasOne('App\Photo', 'id', 'background_id');
    }

    public function photos()
    {
        return $this->hasMany('App\Photo');
    }

    public function getPhotoCountAttribute()
    {
        return $this->photos->count();
    }

}
