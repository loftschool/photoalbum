<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'PageController@index');
Route::get('/KILLTHEMALL', function () {
    DB::statement('SET FOREIGN_KEY_CHECKS=0;');

    DB::table('photos')->truncate();
    DB::table('albums')->truncate();
    DB::table('likes')->truncate();
    DB::table('comments')->truncate();
    DB::table('password_resets')->truncate();
    DB::table('users')->truncate();
    DB::statement('SET FOREIGN_KEY_CHECKS=1;');
});
Route::get('/logout', function () {
    Illuminate\Support\Facades\Auth::logout();
});

Route::auth();

Route::group(['middleware' => 'auth'], function () {
    /* страницы */
    Route::get('/home', 'PageController@home');
    Route::get('/user/{id}', 'PageController@user');
    Route::any('/search', 'PageController@search');
    Route::any('/search/tag/{tag}', 'PageController@searchTag');
    Route::get('/album/{id}', 'PageController@album');

    /* Сохранение данных через ajax */
    Route::post('/user/savedata', 'UserController@saveData');

    /* Альбомы */
    Route::post('/albums/store', 'AlbumController@store');
    Route::post('/albums/update/{id}', 'AlbumController@update');
    Route::get('/albums/destroy/{album_id}', 'AlbumController@destroy');
    Route::post('/albums/updateBackground/{album_id}', 'AlbumController@updateBackground');
    /* Фотографии */

    Route::post('/photos/store/{album_id}', 'PhotoController@store');
    Route::post('/photos/update/{photo_id}', 'PhotoController@update');
    Route::get('/photos/destroy/{photo_id}', 'PhotoController@destroy');
    /* API */
    Route::get('/api/getPhotos/{page}', 'ApiController@getLatestPhotos');
    Route::get('/api/getPhotosByCount/{count}', 'ApiController@getLatestPhotosByCount');
    Route::get('/api/getAlbumPhotos/{id}', 'ApiController@getAlbumPhotos');
    Route::get('/api/likePhoto/{photo_id}', 'LikeController@doLike');
    Route::get('/api/unlikePhoto/{photo_id}', 'LikeController@doUnLike');
    Route::post('/api/newComment/{photo_id}', 'CommentController@store');
    Route::get('/api/test', "ApiController@test");

});
