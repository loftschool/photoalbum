<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use League\Flysystem\Exception;

class UserController extends Controller
{
    public function saveData(Request $request)
    {
        try {
            $user = Auth::user();
            $result = [ 'status' => 'success', 'message' => 'Данные сохранены'];

            if ($request->hasFile('photo')) {
                $this->validate($request, [
                    'photo' => 'image',
                ]);
                $extension = File::extension($request->file('photo')->getClientOriginalName());
                $photo = $request->file('photo')->move('uploads/photos/', $user->id.'.'.$extension);
                $filename = '/'.$photo->__toString().'?'.time();
                $user->photo = $filename;
                $result['photo'] = $filename;
            }

            if ($request->hasFile('background')) {
                $this->validate($request, [
                    'background' => 'image'
                ]);
                $extension = File::extension($request->file('background')->getClientOriginalName());
                $background = $request->file('background')->move('uploads/backgrounds/', $user->id.'.'.$extension);
                $filename = '/'.$background->__toString().'?'.time();
                $user->background = $filename;
                $result['background'] = $filename;
            }
            $user->name = $request->name;
            $user->email = $request->email;
            $user->description = $request->description;
            $user->vk = $request->vk;
            $user->facebook = $request->facebook;
            $user->googleplus = $request->googleplus;

            $user->save();
        } catch (Exception $e) {
            $result = [ 'status' => 'error', 'message' => 'Ошибка сохранения данных'];
            return $result;
        }

        return $result;
    }
}
