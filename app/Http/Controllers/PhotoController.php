<?php

namespace App\Http\Controllers;

use App\Album;
use App\Like;
use App\Photo;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use League\Flysystem\Exception;

class PhotoController extends Controller
{
    public function store(Request $request, $album)
    {
        $user = Auth::user();

        $albumdata = Album::find($album);
        if ($user->id != $albumdata->user_id) {
            return ['error' => 'auth error'];
        }

        $this->validate($request, [
            'cover' => 'required|image',
        ]);

        try {
            $result = DB::transaction(function () use ($user, $album, $request) {
                $result = ['status' => 'success'];
                $photo = new Photo();
                $photo->user_id = $user->id;
                $photo->name = "Фотография без названия";
                $photo->album_id = $album;
                $photo->save();

                $extension = File::extension($request->file('cover')->getClientOriginalName());
                $file = $request->file('cover')->move('uploads/pictures/', $photo->id . '.' . $extension);

                $thumb = '/uploads/pictures/thumbs/' . $photo->id . '.' . $extension;
                Image::make($file)->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path() . $thumb);

                $filename = '/' . $file->__toString() . '?' . time();

                $photo->url = $filename;
                $photo->thumb = $thumb . "?" . time();
                $photo->save();

                $result['album_likes'] = Like::getAlbum($album);

                $result['photo_id'] = $photo->id;
                $result['photo'] = $filename;
                $result['thumb'] = $thumb;
                $result['num'] = $request->input('num');

                return $result;
            });
            return ['status' => 'success', 'result' => $result];
        } catch (Exception $e) {
            return ['status' => 'error'];
        }


    }

    public function update(Request $request, $id)
    {
        try {
            $photo = Photo::find($id);

            $user = Auth::user();
            if ($user->id != $photo->user_id) {
                return ['error' => 'auth error'];
            }
            $photo->name = $request->input('name');
            $photo->description = $request->input('description');
            $photo->save();
            return [ 'id' => $id, 'desc' => $photo->name, 'status' => 'Фотография успешно отредактирована'];
        } catch (Exception $e) {
            return [ 'status' => 'error'];
        }

    }

    public function destroy($id)
    {
        $album = Album::where('background_id', $id)->first();
        if (empty($album)) {
            $photo = Photo::findOrFail($id);
            $user = Auth::user();
            if ($user->id != $photo->user_id) {
                return ['errors' => true, 'result' => 'Ошибка авторизации'];
            }
            $result = preg_split('|\?.*|', $photo->url)[0];
            File::delete(public_path().$result);
            $photo->delete();
            return ['errors' => false, 'data' => $photo];
        } else {
            return [
                'result' => 'Нельзя удалить фотографию,
                 которая является обложкой.
                 Измените обложку для удаления этой фотографии',
                'errors' => true
            ];
        }

    }


}
