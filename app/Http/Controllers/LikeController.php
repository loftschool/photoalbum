<?php

namespace App\Http\Controllers;

use App\Like;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use League\Flysystem\Exception;

class LikeController extends Controller
{
    public function doLike(Request $request, $photo_id)
    {
        $user_id = Auth::user()->id;
        $check = Like::where('photo_id', $photo_id)->where('user_id', $user_id)->count();
        if ($check == 0) {
            $like = new Like();
            $like->photo_id = $photo_id;
            $like->user_id = $user_id;
            $like->save();
            $count = Like::count($photo_id);
            return [
                'result' => 'Вы успешно лайкнули эту фотографию',
                'count' => $count
            ];
        } else {
            $count = Like::count($photo_id);
            return [
                'result' => 'Вы уже лайкали эту фотографию',
                'count' => $count
            ];
        }

    }
    public function doUnLike(Request $request, $photo_id)
    {
        $user_id = Auth::user()->id;
        try {
            $like = Like::where('photo_id', $photo_id)->where('user_id', $user_id)->first();
            if (!empty($like)) {
                $like->delete();
                $count = Like::count($photo_id);
            }
        } catch (Exception $e) {
            $count = Like::count($photo_id);
            return [
                'result' => 'Ошибка удаления лайка',
                'count' => $count
            ];
        }

        return [
            'result' => 'Лайк удален, котик расстроен',
            'count' => $count
        ];
    }
}
