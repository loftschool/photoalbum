<?php

namespace App\Http\Controllers;

use App\Album;
use App\Photo;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Exception;


class AlbumController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'cover' => 'required|image',
        ]);
        try {
            $album = DB::transaction(function () use ($request) {
                $album = new Album();
                $album->name = $request->name;
                $album->description = $request->description;
                $album->user_id = Auth::user()->id;
                $album->save();
                $photoC = new PhotoController();
                $photo = $photoC->store($request, $album->id);

                if ($photo['status']!='success') {
                    throw new Exception('Проблема с фотографией');
                }
                $album->background_id = $photo['result']['photo_id'];
                $album->save();
                $album = $album->toArray();
                $album['photo'] = $photo['result']['photo'];
                return $album;
            });
        } catch (Exception $e) {
            return ['result' => 'error'];
        }

        return json_encode(['result' => 'Альбом создан', 'data' => $album]);

    }


    public function update($id, Request $request)
    {
        $user = Auth::user();
        $album = Album::find($id);
        if ($user->id != $album->user_id) {
            return ['error' => 'auth error'];
        }
        $album->name = $request->name;
        $album->description = $request->description;
        $album->save();

        return json_encode(['result' => 'Альбом '.$album->name.' отредактирован', 'data' => $album]);
    }

    public function updateBackground($id, Request $request)
    {
        $user = Auth::user();
        $album = Album::find($id);
        if ($user->id != $album->user_id) {
            return ['error' => 'auth error'];
        }
        DB::transaction(function () use ($request, $album) {
            if ($request->hasFile('cover')) {
                Photo::destroy($album->background_id);

                $photoC = new PhotoController();
                $photo = $photoC->store($request, $album->id);
                $album->background_id = $photo['result']['photo_id'];
            }

            $album->name = $request->name;
            $album->description = $request->description;
            $album->save();
        });


        return ['result' => 'Альбом '.$album->name.' отредактирован', 'data' => $album];
    }

    public function destroy($id)
    {

        $album = Album::with('photo')->findOrFail($id);

        $user = Auth::user();
        if ($user->id != $album->user_id) {
            return ['error' => 'auth error'];
        }
        try {
            if (!empty($album->photo)) {
                $picture = $album->photo->url;
                $result = preg_split('|\?.*|', $picture)[0];
                File::delete(public_path().$result);
                Photo::destroy($album->photo->id);
            }
            //на случай если что-то со связями в БД
            $album->delete();
            Photo::where('album_id', $id)->delete();

            return $album;
        } catch (Exception $e) {
            return ['result' => 'Ошибка:'. $e->getMessage()];
        }
    }
}
