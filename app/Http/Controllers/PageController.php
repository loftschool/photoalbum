<?php

namespace App\Http\Controllers;

use App\Album;
use App\Comment;
use App\Like;
use App\Photo;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use League\Flysystem\Exception;

class PageController extends Controller
{

    public function index()
    {
        if (Auth::guest()) {
            return view('index');
        } else {
            return redirect('/home');
        }
    }

    public function home(Request $request)
    {
        $user = Auth::user();
        $data['user'] = $user;
        $data['auth_id'] = $user->id;
        $data['photos'] = Photo::latest()->take(6)->with('user')->with('album')->get();
        $data['myalbums'] = Album::latest()->where('user_id', $user->id)->with('photo')->take(8)->get();
        $data['url'] = $request->path();
        return view('home', $data);
    }

    public function user($id, Request $request)
    {
        $user = User::findOrFail($id);
        $data['user'] = $user;
        $data['url'] = $request->path();
        $data['myalbums'] = Album::latest()->where('user_id', $user->id)->with('photo')->take(8)->get();
        $data['authUser'] = Auth::user();
        $data['auth_id'] = $data['authUser']->id;
        return view('user', $data);

    }

    public function album($id, Request $request)
    {
        $album = Album::findOrFail($id);

        $photos = Photo::where('album_id', $id)
            ->latest()
            ->get();
        if ($photos->count() < 1) {
            abort(404);
        }
        $onePhoto = $photos->first();
        $photoIds = $photos->pluck('id');
        $likes = Like::whereIn('photo_id', $photoIds)->count();
        $comments = Comment::whereIn('photo_id', $photoIds)->count();
        $user = User::find($onePhoto->user_id);

        $data['url'] = $request->path();
        $data['likes'] = $likes;
        $data['comments'] = $comments;
        $data['album'] = $album;
        $data['photos'] = $photos;
        $data['user'] = $user;
        return view('showAlbum', $data);
    }

    public function search(Request $request)
    {
        $user = Auth::user();
        $data['user'] = $user;
        $data['auth_id'] = $user->id;
        $data['url'] = $request->path();
        $search = $request->input('s');
        $data['search'] = $search = urldecode($search);
        $data['photos'] = Photo::with('user')
            ->where('name', 'LIKE', "%$search%")
            ->orWhere('description', 'LIKE', "%$search%")
            ->get();

        $data['albums'] = Album::with('user')
            ->with('photo')
            ->where('name', 'LIKE', "%$search%")
            ->orWhere('description', 'LIKE', "%$search%")
            ->get();

        return view('search', $data);
    }

    public function searchTag(Request $request, $tag)
    {
        $user = Auth::user();
        $data['user'] = $user;
        $data['auth_id'] = $user->id;
        $data['url'] = $request->path();
        $data['tag'] = '#';
        $data['search'] = $tag = urldecode($tag);
        $data['photos'] = Photo::with('user')
            ->Where('description', 'LIKE', "%#$tag%")
            ->get();

        return view('search', $data);
    }
}
