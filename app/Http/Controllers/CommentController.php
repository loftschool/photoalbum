<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function store(Request $request, $photo_id)
    {
        $user = Auth::user();
        $user_id = $user->id;
        $comment = new Comment();
        $comment->user_id = $user_id;
        $comment->comment = $request->comment;
        $comment->photo_id = $photo_id;
        $comment->save();

        return [
            'photo' => $user->photo,
            'id' => $user_id,
            'name' => $user->name
        ];
    }

}
