<?php

namespace App\Http\Controllers;

use App\Photo;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{
    public function getLatestPhotos($page)
    {
        $perpage = 6;
        $total = Photo::count();
        $totalpages = $total / $perpage;
        $photos = Photo::with('user')
            ->with('comments')
            ->with('album')
            ->with('likes')
            ->latest()->forPage($page, $perpage)->get();
        $data['total'] = $total;
        $data['totalpages'] = ceil($totalpages);
        $data['photos'] = $photos;
        return $data;
    }

    public function getLatestPhotosByCount($count)
    {
        $photos = Photo::with('user')
            ->with('comments')
            ->with('likes')
            ->with('album')
            ->latest()->take($count)->get();
        $data['photos'] = $photos;
        return $data;
    }


    public function getAlbumPhotos($id)
    {
        $photos = Photo::with('user')
            ->with('comments')
            ->with('album')
            ->latest()
            ->where('album_id', $id)
            ->with(['likes' => function ($q) {
                $q->where('user_id', Auth::user()->id);
            }])
            ->get();
        return $photos;
    }

    public function test(Request $request)
    {
        return $request->all();
    }
}
