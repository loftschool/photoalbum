<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    public static function getAlbum($album)
    {
        $photos = Photo::where('album_id', $album)->get(['id']);
        return Like::whereIn('photo_id', $photos)->count();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function photo()
    {
        return $this->belongsTo('App\Photo');
    }

    public static function count($photo_id)
    {
        return Like::where('photo_id', $photo_id)->count();
    }
}
