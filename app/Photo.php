<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    protected $appends = ['Likes_count', 'Comments_count'];

    public function user()
    {
        return $this->belongsTo('App\User')->select(['id', 'name', 'photo']);
    }

    public function album()
    {
        return $this->belongsTo('App\Album')->select(['id', 'name']);
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'photo_id', 'id')->with('user');
    }

    public function likes()
    {
        return $this->hasMany('App\Like', 'photo_id', 'id');
    }

    public function getCommentsCountAttribute()
    {
        return $this->comments()->count();
    }

    public function getLikesCountAttribute()
    {
        return $this->likes()->count();
    }

}
