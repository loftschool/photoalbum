'use strict';

var fs = require('fs');

/* --------- plugins --------- */
var	gulp        	= require('gulp');
var	jade        	= require('gulp-jade');
var sass			= require('gulp-sass');
var	browserSync 	= require('browser-sync').create();
var	uglify      	= require('gulp-uglify');
var	rename      	= require("gulp-rename");
var	browserify  	= require('gulp-browserify');
var	plumber     	= require('gulp-plumber');
var	useref      	= require('gulp-useref');
var	gulpif      	= require('gulp-if');
var cssnano     	= require('gulp-cssnano');
var autoprefixer 	= require('gulp-autoprefixer');
var sourcemaps 		= require('gulp-sourcemaps');
var shell			= require('gulp-shell');
var concat 			= require('gulp-concat');
var sassGlob        = require('gulp-sass-glob');

/* --------- paths --------- */
var DEV_PATH = 'htmlonly/_dev';
var	paths = {
	jade : {
		location    : DEV_PATH + '/markups/**/*.jade',
		compiled    : DEV_PATH + '/markups/_pages/*.jade',
		destination : './htmlonly/pages'
	},

	scss : {
		location    : DEV_PATH + '/styles/**/*.scss',
		entryPoint  : DEV_PATH + '/styles/main.scss',
		destination : './public/css/'
	},

	js : {
		location    : DEV_PATH + '/scripts/**/*.js',
		entyPoint   : DEV_PATH + '/scripts/main.js',
		destination : './public/js/'
	},

	browserSync : {
		baseDir : './',
		watchPaths : [DEV_PATH + '/markups/_pages/*.jade', 'css/*.css', 'js/*.js']
	},

	indexBuilder : {
		location : DEV_PATH + '/index_builder.js'
	}
}

/* --------- jade --------- */
gulp.task('jade', function() {
	var assets = useref.assets();

	gulp.src(paths.jade.compiled)
		.pipe(plumber())
		.pipe(jade({
			pretty: '\t',
			// locals: JSON.parse(fs.readFileSync(YOUR_LOCALS, 'utf8')),
			basedir : './'
		}))
		.pipe(assets)
		.pipe(gulpif('*.js', uglify()))
		.pipe(gulpif('*.css', cssnano()))
		.pipe(assets.restore())
		.pipe(useref())
		.pipe(gulp.dest(paths.jade.destination));
});

/* --------- sass --------- */
gulp.task('sass', function() {
	return gulp.src(paths.scss.entryPoint)
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(sassGlob())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(sourcemaps.write())
		.pipe(cssnano())
		.pipe(rename('main.css'))
		.pipe(gulp.dest(paths.scss.destination))
});

/* --------- browser sync --------- */
gulp.task('sync', function() {
	browserSync.init({
		server: {
			baseDir: paths.browserSync.baseDir
		}
	});
});

/* --------- scripts --------- */
gulp.task('scripts', function() {
	return gulp.src(paths.js.entyPoint)
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(browserify({
			insertGlobals : true,
			debug : true,
			transform : ['require-globify']
		}))
		.pipe(uglify())
		.pipe(rename('main.min.js'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(paths.js.destination));
});

/* --------- index-builder --------- */
gulp.task('exec', shell.task("node " + paths.indexBuilder.location));

/* --------- watch --------- */
gulp.task('watch', function(){
	// gulp.watch(paths.jade.location, ['jade']);
	gulp.watch(paths.scss.location, ['sass']);
	gulp.watch(paths.js.location, ['scripts']);
	gulp.watch(paths.browserSync.watchPaths).on('change', browserSync.reload);
});

/* --------- default --------- */
gulp.task('default', ['sass', 'scripts', 'watch']);