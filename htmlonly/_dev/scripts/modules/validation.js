var modals = require('./modals.js');

module.exports = {
    check: function (form) {
		var errorsBlock = form.closest('.popup').find('.popup-errors ul');
	    errorsBlock.html('');

	    var types = {
	        text : function (field, name) {
		        if (field.val() == '') {
			        field.addClass('error');

			        var message = $('<li>', {
				        text : 'Заполните поле ' + name
			        });
			        errorsBlock.append(message);

		        } else {
			        field.removeClass('error');
		        }
	        },

		    file : function (field) {

			    var inputFile = form.find('[type=file]');
			    var btn = form.find('.error-attach');

			    if (!inputFile.val()) {
				    btn.addClass('error');

				    var message = $('<li>', {
					    text : "Добавьте файлы для загрузки"
				    });

				    errorsBlock.append(message);
			    } else {
				    btn.removeClass('error');
			    }
		    }
        }
	    
	    form.find('[data-valid-type]').each(function () {
			var
				field = $(this),
				type = field.data('valid-type'),
				name = field.data('valid-name');
		    
		    types[type](field, name);

		    field.on('keyup', function (e) {
			    e.preventDefault();
				$(this).removeClass('error');
		    });
        });

	    return !!!form.find('.error').length
    }
}