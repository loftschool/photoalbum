// modules
var handlebars = require('handlebars');
var modals = require('./modals.js');
var validation = require('./validation.js');

// globals
var albumId = $('#album-id').val();
var token = $('#csrf').val(); // токен необходимо передавать при каждом запросе
var fileInput = $('#dropzone');
var filesList = {}; // для хранения файлов, так как стандартный объект - readonly
var formWidget = $('.fileupload__widget');
var placeholder = $('#upload-desc');

var renderFile = function (files, callback) {
	var i = 0;

	function loadFile() {
		var
			file = files[i],
			reader = new FileReader();

		if (file) {
			reader.readAsDataURL(file);

			reader.onload = function (e) {

				var fileSize = e.total / 1024 / 1024;

				if (fileSize <= 2 ) {
					var template = handlebars.compile($('#photo-template').html()),
						photoData = template({
							cover : e.target.result,
							ident : i,
							removeBtn : true
						});
					$('#fileupload-photos').append(photoData);

				} else {
					var template = handlebars.compile($('#photo-template').html()),
						photoData = template({
							cover : e.target.result,
							ident : i,
							removeBtn : false
						});

					$('#unloaded-photos').append(photoData);
				}
			}

			reader.onloadend = function (e) {
				var fileSize = e.total / 1024 / 1024;

				if (files.length == i + 1) {
					if (callback && typeof callback == 'function') {
						callback();
					}
				}

				if (fileSize <= 2) {
					filesList[i] = file;
				}

				file.num = i;
				i++;
				loadFile();
			}
		}
	}

	loadFile();
}

module.exports = {
	showPopup : function () {
		$('a[href="#fileupload"]').modal({
			onClose : function () {
				$('#dropzone').val('');
				$('.fileupload__desc').show();
				$('.fileupload__photos').empty();
				$('#unload-alert').hide();
			}
		});
	},

	init: function () {

		// визуальный аплоад (не на сервер)
		fileInput.on('change', function (e) {
			var
				$this = $(this),
				files = $this.prop('files'),
				container = $this.closest('.fileupload');

			fileInput.prop('disabled', true); // что бы не отрабатывал клик по инпут файлу
			placeholder.hide();

			renderFile(files, function () {
				if ($('#unloaded-photos li').length) {
					$('#unload-alert').show();
				}
			});

		});

		//удаляем картинку
		$('body').on('click', '.fileupload__photo-remove', function(e){
			var
				$this = $(this),
				item = $this.closest('.fileupload__photos-item'),
				id = parseInt(item.data('id'));

			delete filesList[id];
			item.fadeOut(function () {
				$(this).remove();
			});
		});

		// включамем инпут при закрытиях всплывашки и подобном
		$(document).on('click', function(e){
			var target = $(e.target);
			if (!target.closest('#fileupload').length) {
				fileInput.prop('disabled', false);
			}
		});

		$('#upload-form').on('submit', function(e){
			e.preventDefault();
			var
				form = $(this),
				formData = new FormData(),
				files = filesList,
				loaded = $.Deferred();

			formData.append('_token', token);

			if (validation.check(form)) {
				var errors = false;

				for (var file in files) {
					var
						$this = files[file];

					formData.append('cover', $this);
					formData.append('num', $this.num);

					$.ajax({
						url: "/photos/store/" + albumId,
						method: "post",
						data: formData,
						dataType: 'json',
						processData: false,
						contentType: false,
						beforeSend: function () {
							$('#add-photos-btn').addClass('preload');
							form.closest('.popup').addClass('preload');
						},
						success: function (data) {
							$('#add-photos-btn').removeClass('preload');

							if (data.status == 'success') {
								var list = $('.projects_albums');

								// $('.fileupload__photos-item[data-id=' + data.num +']').remove();

								$('#fileupload-photos .fileupload__photos-item').last().remove();

								console.log(data);

								var photoTemplate = handlebars.compile($('#photo-item-template').html()),
									photoItem = photoTemplate({
										id : data.result['photo_id'],
										cover : data.result['photo']
									});

								$('.photo-counter').text(list.find('.projects__item').length + 1); // общее количество фоток
								
								if (!$('.projects__item_newbie', list).length) {
									list.prepend(photoItem);
									console.log('first');
								} else {
									$('.projects__item_newbie', list).after(photoItem);
									console.log('others');
								}

								if (!errors) {
									if (!$('#fileupload-photos .fileupload__photos-item').length) {
										loaded.resolve();
									}
								} else {
									if (!$('#fileupload-photos .fileupload__photos-item').length) {
										loaded.reject();
									}
								}

							}
						}, 
						
						error : function (xhr, status, error) {
							if (errors == false) {
								errors = true;
							}
							$('#fileupload-photos .fileupload__photos-item').last().remove();
						}
					});
				}
			}

			loaded.then(function () {
				modals.alert('Файлы были загружены');
				window.location.reload();
			}, function () {
				modals.alert('Не все файлы были загружены');
			});

		});

		$('#add-photos-btn').on('click', function(e){
		    e.preventDefault();

			$('#upload-form').trigger('submit');
		});

		// drag

		if (formWidget.length) {
			formWidget.on({
				dragover : function (e) {
					e.preventDefault();
					$(this).css({
						'border-color' : '#28bbf0'
					})
				},

				dragleave : function (e) {
					e.preventDefault();
					$(this).css({
						'border-color' : '#f1f1f1'
					})
				}
			});

			var elem = document.getElementById('upload-form');
			elem.ondrop = function (e) {
				e.preventDefault();

				formWidget.css('border-color', '#f1f1f1');
				placeholder.hide();
				fileInput.prop('disabled', true);
				renderFile(e.dataTransfer.files);
			}
		}

	}
}
