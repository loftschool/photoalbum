/**
 * Callbacks :
 * beforeShow - до появления всплывашки
 * onLoad - при загрузки всплывашки
 * afterParse - после получения параметров из data-params (доступны в первой пермеменной)
 * onClose - при скрывании всплывашки
 * @param options
 * @returns {*}
 */

$.fn.modal = function(options) {
	var body = $('body'),
		elem = this;

	var methods = (function() {

		var getParams = function (str) {
			return str.replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
		}

		var
			popups = $('.popup'),
			duration = 300;

		return {
			open : function (id, params, afterParse) {
				var reqPopup = popups.filter(id),
					data = params ? getParams(params) : "";

				console.log('open method');
				
				popups.fadeOut();
				body.removeClass('locked');

				reqPopup.fadeIn(duration, function() {
					console.log('fade in')
					if (options && typeof options.onLoad == 'function') {
						options.onLoad();
					}
				});

				if (afterParse && typeof afterParse == 'function') {
					afterParse.call(reqPopup, data);
				}

				body.addClass('locked');
			},

			close : function (popup) {

				popup = popup || popups;
				popups.find('.popup-errors ul').html('');
				popups.find('.error').removeClass('error');

				popup.fadeOut(duration, function () {
					body.removeClass('locked')
					popups.removeClass('preload');
					popups.find('textarea, input:not([type=submit])').val('');
					popup.find('.popup-alert').remove();

					if (popup && options && typeof options.onClose == 'function') {
						options.onClose();
					}
				});
			}
		}
	}());

	// предача аргументов в метод
	if (methods[options]) {
		return methods[options].apply(this, [].slice.call(arguments, 1));
	}

	// закрыть встпылвашки
	$('.popup__close, .close-trigger').on('click', function (e) {
		e.preventDefault();
		var popup = $(this).closest('.popup');
		methods.close(popup);
	});

	// закрыть по клику вне всплывашки
	$(document).on('click', function(e) {
		if ($(e.target).hasClass('popup__overlay')) {
			methods.close();
		}
	});


	elem.off('click');

	body.on('click', elem.selector, function (e) {

		e.preventDefault();

		var
			$this = $(this),
			id = $this.attr('href'),
			params = $this.attr('data-params'),
			callback = '';

		methods.close();

		if (options && typeof options.beforeShow == 'function') {
			options.beforeShow();
		}

		if (options && typeof options.afterParse === 'function') {
			callback = options.afterParse
		}

		methods.open(id, params, callback);
	});


}
