var stickThisUp = function (wScroll) {
	var
		$this = $('.edit-btn_add').first(),
		position = $this.closest('.albums__add-btn').offset().top,
		scrollTop = wScroll + 150,
		stickyStart = scrollTop >= position,
		clone = $this.clone();

	var container = $('<div>', {
		attr : {
			class : 'container'
		},
		html : clone
	});

	var markup = $('<div>', {
		attr : {
			class : 'sticky-plus'
		},
		css : {
			position : 'fixed',
			top : '150px',
			left : '0',
			right: '0'
		},
		html : container
	});

	if (stickyStart) {
		$this.hide();
		if (!$('.sticky-plus').length) {
			$('body').append(markup);
		}
	} else {
		$this.show();
		$('.sticky-plus').remove();
	}
}

module.exports = {
	stickOnLoad : function (wScroll) {

		if ($('.edit-btn_add').length) stickThisUp(wScroll);

	},
	
	stickOnScroll : function (wScroll) {

		if ($('.edit-btn_add').length) stickThisUp(wScroll);

	},
	
}