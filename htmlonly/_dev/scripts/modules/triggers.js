module.exports = {
    loginBtn: function () {
       if (window.location.hash == '#register') {
	       $('.auth__block').addClass('auth__block_login');
       }

        $('.flip-trigger').on('click', function(e){
	        var container = $(this).closest('.auth__block'),
		        otherSide = $('.auth__block-back', container);
	        
			if (otherSide.length) {
				e.preventDefault();
				$('.auth__block').toggleClass('auth__block_login');
			}
        });
    },
	
	upBtn: function () {
		$('.up-btn').on('click', function(e){
			e.preventDefault();
			$('body, html').animate({scrollTop : 0}, 500);
		});
	}
}