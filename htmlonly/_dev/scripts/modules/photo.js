var modals = require('./modals');

module.exports = {
	remove: function () {
		$('body').on('click', '#remove-photo', function (e) {
			e.preventDefault();
			var
				id = $('.form-container__id').val(),
				url = '/photos/destroy/' + id,
				popup = $(this).closest('.popup');

			modals.confirmMessage('Вы хотите удалить эту фотографию?', popup, function () {
				$.ajax({
					url: url,
					type: 'get',
					success: function (request) {
						console.log(request);
						if (!request.errors) {
							var id = request.data.id;

							$.fn.modal('close');

							$('.projects_albums .projects__item[data-id=' + id + ']')
								.fadeOut(function () {
									$(this).remove();
								});

							window.location.reload();
						} else {
							modals.alert(request.result);
						}
					}
				});
			});
		});
	}
}