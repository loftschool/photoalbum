require('./popup-creator.js');
var handlebars = require('handlebars');

module.exports = {

	alert : function (message) {
		var messageText = 'message=' + message;

		$.fn.modal('open', '#alert-popup',
			messageText,
			function (params) {
				$(this).find('.alert-message').text(params.message);
			}
		);
	},

	confirmMessage : function (text, popup, callback) {
		var alert = handlebars.compile($('#popup-alert').html()),
			alertHtml = alert({
				text : text
			});

		console.log('confirm message');

		popup
			.find('.popup__content')
			.append(alertHtml);

		var decision = '';

		$('body').one('click', '.popup-alert-btn', function(e){
		    e.preventDefault();

			console.log('confirm message event');

			var $this = $(this),
				type = $this.attr('href');

			decision = type == '#yes' ? true : false;

			if (!decision) {
				popup.find('.popup-alert').remove();
			} else if (typeof callback == 'function') {
				callback();
			}
		});
	},
	
	headerMessage : function (alerts) {
		var messagesTemplate = handlebars.compile($('#header-alerts-template').html()),
			messages = messagesTemplate({
				messages : [
					alerts
				]
			});

		var timer;

		$('#header-alerts')
			.addClass('active')
			.find('.header__alerts-content')
			.html(messages);

		if (typeof timer != 'undefined') {
			clearTimeout(timer);
		}

		setTimeout(function () {
			$('#header-alerts').removeClass('active');
		}, 1500);

	}
}
