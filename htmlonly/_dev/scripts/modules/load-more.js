var handlebars = require('handlebars');

module.exports = {
    load: function () {
	    var page = 2; //По умолчанию одна страница загружена, поэтому грузим сразу №2

	    $('.proceed .proceed__btn').on('click', function(e){
		    e.preventDefault();

		    var btn = $(this),
			    container = btn.closest('.section').find('.projects');

		    $.ajax({
			    url : '/api/getPhotos/'+page,
			    type : "GET",
			    beforeSend : function () {
					btn.addClass('preload');
			    },

			    success: function (data) {
				    var projectMarkup = $('#project-template').html(),
					    template = handlebars.compile(projectMarkup),
					    projectData = template(data);

				    page++;
				    btn.removeClass('preload');

				    if (page > data.totalpages) {
					    btn.remove();
				    }

				    container.append(projectData);
			    }
		    });
	    });
    }
}