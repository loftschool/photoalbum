module.exports = {
	// показать картинку сразу после выбора
	renderFile : function (input, block) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				block.css('background-image', 'url(' + e.target.result + ')');
			}

			reader.readAsDataURL(input.files[0]);
		}
	}
}