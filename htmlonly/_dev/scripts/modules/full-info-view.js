require('./popup-creator.js');
var handlebars = require('handlebars');
var csrf = $('#csrf').val();

var getCurrentItem = function (object, id) {
	for (var obj in object) {
		var $this = object[obj];

		if ($this.id == id) {
			return $this;
		}
	}
}

var parseHashTags = function (text) {
	return text.replace(/(^|\s)#([\w\u0430-\u044f]+)/g, '$1<a href="/search/tag/$2">#$2</a>');
}

var addComment = function (block) {
	block
		.closest('.projects__item')
		.find('.project-activity__elem').eq(0)
		.find('.project-activity__counter')
		.text($('.comments__item').length);
}

var addLike = function (block, likes) {
	block
		.closest('.projects__item')
		.find('.project-activity__elem').eq(1)
		.find('.project-activity__counter')
		.text(likes);
}

var countUserActivity = function (eq, block) {
	var total = 0;

	$('.project-activity').each(function () {
		var $this = $(this),
			reqBlock = $this.find('.project-activity__elem').eq(eq),
			amount = parseInt(reqBlock.find('.project-activity__counter').text());

		total += amount;
	});

	block.text(total);
}

module.exports = {
	view : function () {

		var
			body = $('body'),
			popupContent = $('#pic-view .pic-view__container');

		$('body').on('click', '.projects__pic:not(.show-page)', function(e){
			e.preventDefault();

			var
				$this = $(this),
				id = $this.data('id'),
				photoId = $this.data('photo-id'),
				amount = $this.closest('.projects').find('.projects__item').length,
				isAlbum = $this.hasClass('show-album');

			var settings = {
				url : isAlbum ? '/api/getAlbumPhotos/' + id : 'api/getPhotosByCount/' + amount
			}

			$.ajax({
				url : settings.url,
				type : 'get',
				beforeSend : function () {
					$this.addClass('preload');
				},
				success : function (data) {

					var dataArray = isAlbum ? data : data.photos,
						photoNum = isAlbum ? photoId : id;

					var	firstItem = getCurrentItem(dataArray, photoNum);

					var	template = handlebars.compile($('#album-template').html()),
						markup = template({
							items : dataArray,
							username : firstItem.user.name,
							userphoto : firstItem.user.photo,
							photoId: firstItem.id,
							comments : firstItem.comments,
							liked : firstItem.likes.length,
							likesAmount: firstItem['Likes_count'],
							title : firstItem.name,
							desc : parseHashTags(firstItem.description)
						});

					var imagesLoaded = $.Deferred();

					popupContent
						.html(markup)
						.find('.pic-view__image:first-child img')
						.on('load', function () {
							imagesLoaded.resolve();
						});

					imagesLoaded.done(function () {
						$this.removeClass('preload');

						$.fn.modal('open', '#pic-view', '', function () {
							var owl = $('.pic-view__images').owlCarousel({
								items    : 1,
								loop     : true,
								margin   : 10,
								autoHeight: true,
								onTranslate : function () {
									$('.comments__list').addClass('preload');
								},

								onInitialized : function () {
									var slideIndex = 0;

									// выставление нужной фотки по открытию
									$('.owl-item:not(.cloned)').each(function (index) {
										var $this = $(this),
											elem = $this.find('.pic-view__image'),
											elemId = elem.data('id');

										if (elemId == photoNum) {
											slideIndex = index;
										}
									});

									setTimeout(function () {
										$('.pic-view__images').trigger('to.owl.carousel', [slideIndex, 0]);
									}, 50);
								},
								// смена данных о фотке по листанию
								onTranslated : function () {
									var
										$this = $(this)[0],
										list = $this['$element'][0],
										currentItem = $this._current,
										item = $(list).find('.pic-view__image').eq(currentItem),
										id = item.data('id'),
										currentData = getCurrentItem(dataArray, id),
										commentsList = $('.comments__list');

									commentsList.find('li').remove();
									$('.comments__add-input').val('');

									currentData['comments'].forEach(function (item) {
										var
											comment = handlebars.compile($('#comments-template').html()),
											commentHtml = comment({
												comment : item.comment,
												userphoto : item.user.photo,
												username : item.user.name
											});

										commentsList.append(commentHtml);
									});

									var like = handlebars.compile($('#like-template').html()),
										likeMarkup = like({
											liked : currentData['likes'].length,
											likesAmount : currentData['Likes_count']
										});

									$('.likes-container').html(likeMarkup);

									var about = handlebars.compile($('#description-template').html()),
										aboutMarkup = about({
											title : currentData['name'],
											desc : parseHashTags(currentData['description'])
										});

									$('#photo-about').html(aboutMarkup);

									var user = handlebars.compile($('#user-info').html()),
										userMarkup = user({
											userphoto : currentData['user']['photo'],
											username : currentData['user']['name']
										});

									$('#user-ident').html(userMarkup);

									commentsList.removeClass('preload');
									$('#photo-id').val(id);
								}
							});

							owl.trigger('refresh.owl.carousel');

							$('.pic-view__gallery-btn_right').click( function(e) {
								e.preventDefault();
								owl.trigger('next.owl.carousel');
							});

							$('.pic-view__gallery-btn_left').click( function(e) {
								e.preventDefault();
								owl.trigger('prev.owl.carousel');
							});

							$('.pic-view__image').on('click', function(e){
							    e.preventDefault();
								owl.trigger('next.owl.carousel');
							});
						});
					});
				}
			});
		});
	},

	comments: function () {
		$('body').on('submit', '#add-comment', function(e){
			e.preventDefault();

			var
				form = $(this),
				comment = form.find('[name=comment]').val(),
				id = $('#photo-id').val(),
				url = form.attr('action') + id,
				submit = form.find('[type=submit]');

			$.ajax({
				url : url,
				type : 'POST',
				data : {
					comment : comment,
					_token : csrf
				},
				beforeSend : function () {
					submit.addClass('preload');
				},
				success : function (data) {
					var template = handlebars.compile($('#comments-template').html()),
						commentHTML = template({
							userphoto : data.photo,
							username : data.name,
							comment : comment
						});

					addComment($('.projects__pic[data-photo-id=' + id + ']')); // в альбомах
					addComment(	$('.projects__pic[data-id=' + id + ']'));

					countUserActivity(0, $('.comments-counter'));

					$('.comments__list').append(commentHTML);
					submit.removeClass('preload');
				}
			});
		});
	},

	likes : function () {
		$('body').on('click', '.likes', function(e){
			e.preventDefault();

			var
				btn = $(this),
				id = $('#photo-id').val();

			if (!btn.hasClass('liked')) {
				$.ajax({
					url: '/api/likePhoto/' + id,
					type : 'GET',
					beforeSend : function () {
						btn.addClass('liked');
					},
					success : function (data) {
						btn.find('.likes__amount').text(data.count);
						addLike($('.projects__pic[data-id=' + id + ']'), data.count);
						addLike($('.projects__pic[data-photo-id=' + id + ']'), data.count);

						countUserActivity(1, $('.likes-counter'));
					}
				});
			} else {
				$.ajax({
					url: '/api/unlikePhoto/' + id,
					type : 'GET',
					beforeSend : function () {
						btn.removeClass('liked');
					},
					success : function (data) {

						btn.find('.likes__amount').text(data.count);

						addLike($('.projects__pic[data-id=' + id + ']'), data.count);
						addLike($('.projects__pic[data-photo-id=' + id + ']'), data.count);
						countUserActivity(1, $('.likes-counter'));
					}
				});
			}
		});
	}
}