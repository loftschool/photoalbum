var modals = require('./modals');
var helpers = require('./helpers');

var header = $('.header'),
	page = $('.page-content, .footer'),
	csrf = $('#csrf').val(); // токен необходимо передавать в каждом запросе

//exports
module.exports = {
    edit: function () {
        $('.header__edit-btn').on('click', function(e){
            e.preventDefault();

            page.addClass('disabled');
            header.addClass('header_edit');
        });
    },

    cancel : function () {
        $('.edit-bar__btn_cancel').on('click', function(e){
            e.preventDefault();

            page.removeClass('disabled');
            header.removeClass('header_edit');
        });
    },
	
	changeAvatar : function () {
		$('.photo-edit__input-file').on('change', function(e){
		    e.preventDefault();

			var $this = $(this),
				block = $this.closest('.user').find('.photo-edit_attached');

			helpers.renderFile(this, block);

		});
	},

	changeBg : function () {
		$('.photo-edit__file').on('change', function (e) {
			e.preventDefault();

			helpers.renderFile(this, $(this).closest('.header'));
			helpers.renderFile(this, $('.footer'));
			helpers.renderFile(this, $('#album-cover').find('.projects__pic'));
		});
	},

	saveMainHeader: function () {
		$('.saveUserData').on('click', function(e){
			e.preventDefault();
			var form = $('.edit-header-form'),
				_token = form.find('input[name=_token]').val(),
				name = form.find('input[name=name]').val(),
				description = form.find('textarea[name=description]').val(),
				vk = form.find('input[name=vk]').val(),
				facebook = form.find('input[name=facebook]').val(),
				twitter = form.find('input[name=twitter]').val(),
				googleplus = form.find('input[name=googleplus]').val(),
				email = form.find('input[name=email]').val(),
				photo = form.find('input[name=photo]').prop('files')[0],
				background = form.find('input[name=background]').prop('files')[0],
				formdata = new FormData();


			formdata.append('_token', _token);
			formdata.append('name', name);
			formdata.append('description', description);
			formdata.append('background', background);
			formdata.append('photo', photo);
			formdata.append('vk', vk);
			formdata.append('facebook', facebook);
			formdata.append('twitter', twitter);
			formdata.append('googleplus', googleplus);
			formdata.append('email', email);

			$.ajax({
				url: "/user/savedata",
				method: "POST",
				dataType: 'json',
				processData: false,
				contentType: false,
				data: formdata,
				success : function (data) {
					if (data.status=="success") {
						$('.user__name_value').html(name);
						$('.user__desc_value').html(description);
						$('.user__vk').attr('href', vk);
						$('.user__facebook').attr('href', facebook);
						$('.user__twitter').attr('href', twitter);
						$('.user__googleplus').attr('href', googleplus);
						$('.user__email').attr('href', 'mailto:' + email);

						if (typeof data.photo != 'undefined') {
							$('.user__pic-avatar').attr('style', "background-image: url('" + data.photo + "')")
						}

						if (typeof data.background != 'undefined') {
							$('header').attr('style', "background-image: url('" + data.background + "')")
						}
						$('.page-content, .footer').removeClass('disabled');
						$('header').removeClass('header_edit');

						modals.headerMessage({
							text : data.message
						})

						window.location.reload();
					}
				},

				error : function () {
					console.log('error');

					modals.headerMessage({
						text : 'Файл должен быть изображением'
					})
				}
			})
		});
	},

	saveAlbumHeader : function () {
		$('#save-header').on('click', function(e){
			e.preventDefault();

			var
				$this = $(this),
				id = $this.data('album'),
				container = $this.closest('header'),
				name = container.find('[name=name]').val(),
				description = container.find('[name=description]').val(),
				cover = container.find('[type=file]').prop('files')[0],
				formData = new FormData();

			formData.append('_token', csrf);

			if (cover) {
				formData.append('cover', cover);
			}
			formData.append('name', name);
			formData.append('description', description);

			$.ajax({
				url: '/albums/updateBackground/' + id,
				method: "post",
				data: formData,
				processData: false,
				contentType: false,
				beforeSend : function () {
					header.addClass('preload');
				},
				success : function (data) {
					header.removeClass('preload header_edit');
					$('.page-content').removeClass('disabled');
					$('.footer').removeClass('disabled');

					$('.header-inner__title').text(name);
					$('.header-inner__desc').text(description);

					modals.headerMessage(
						{
							text : data.result
						}
					);

					window.location.reload();
				},

				error : function () {
					header.removeClass('preload');

					modals.headerMessage(
						{
							text : 'Файл должен быть изображением'
						}
					);
				}
			});
		});
	},

	socials : function () {
		$('.socials__link').on({
			mouseenter : function (e) {
				$(this).addClass('active');
			},
			mouseleave : function (e) {
				$(this).removeClass('active');
			}
		});

		$('.socials-save').on('click', function(e){
			e.preventDefault();
			$(this).closest('.socials__link').removeClass('active');

			modals.headerMessage({
				text : 'Данные изменены'
			})
		});
	},
}