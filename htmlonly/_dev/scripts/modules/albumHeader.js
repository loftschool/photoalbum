var showHeader = function(wScroll) {
	var
		header = $('.header__bar'),
		fixedHeader = $('.fixed-header'),
		headerContent = header.find('.header__bar-inner'),
		startPos = header.offset().top;

	if (wScroll > startPos) {
		fixedHeader.addClass('active');
		header.fadeTo(0, 0);
		headerContent.addClass('active');
	} else {
		fixedHeader.removeClass('active');
		header.fadeTo(0, 1);
		headerContent.removeClass('active');
	}
}

module.exports = {
	stickOnScroll : function (wScroll) {
		if ($('.fixed-header').length) showHeader(wScroll);
	},

	stickOnLoad : function (wScroll) {
		if ($('.fixed-header').length) showHeader(wScroll);
	}
}

