var modals = require('./modals');
var handlebars = require('handlebars');
var validation = require('./validation');
var helpers = require('./helpers');

var _token = $('#csrf').val();

module.exports = {
	showAddModal : function () {
		$('a[href="#add-album"]').modal();
	},

	save: function () {
		// сохранить загруженый альбом
		$('.store_album').on('submit', function (e) {
			e.preventDefault();

			var form = $(this),
				name = form.find('input[name=name]').val(),
				description = form.find('textarea[name=description]').val(),
				cover = form.find('input[name=cover]').prop('files')[0],
				formdata = new FormData(),
				list = $('.projects_albums'),
				placeholder = list.closest('section').find('.projects__empty-alert');

			formdata.append('_token', _token);
			formdata.append('name', name);
			formdata.append('description', description);
			formdata.append('cover', cover);

			if (validation.check(form)) {
				$.ajax({
					url: "/albums/store",
					method: "POST",
					dataType: 'json',
					processData: false,
					contentType: false,
					data: formdata,
					beforeSend: function () {
						form.closest('.popup').addClass('preload');
					},
					success: function (data) {
						var albumInfo = $('#album-item-template').html(),
							template = handlebars.compile(albumInfo),
							albumData = template(data.data);

						placeholder.hide();
						modals.alert(data.result);
						list.prepend(albumData);
						window.location.reload();
					},
					error: function () {
						form.closest('.popup').removeClass('preload');

						modals.alert('Файл для обложки должен быть картинкой')
					}
				})
			}
		});

		//изменить обложку при загруке картинки
		$('#add-album .file-input__elem').on('change', function () {
			helpers.renderFile(this, $('#add-album .user-avatar'));
		});
	},

	edit : function () {
		$('.show-edit').modal({
			afterParse: function(params) {
				var $this = $(this);

				$this.find('[name=name]').val(params.name);
				$this.find('[name=description]').val(params.description);
				$this.find('[name=id]').val(params.id);
			}
		});
	},

	remove : function () {
		$('#remove-album').on('click', function(e){
			e.preventDefault();

			var
				$this = $(this),
				form = $this.closest('form'),
				id = $('.form-container__id').val(),
				url = '/albums/destroy/' + id,
				popup = $this.closest('.popup');

			modals.confirmMessage('Вы действительно хотите удалить этот альбом?', popup, function () {
				$.ajax({
					url : url,
					type : 'GET',
					success : function (data) {
						var id = data.id;

						$.fn.modal('close');

						$('.projects_albums .projects__item[data-id=' + id +']')
							.fadeOut(function () {
								$(this).remove();
							});
					}
				});
			});
		});
	},

	addNew : function () {
		$('#edit-album').on('submit', function (e) {
			e.preventDefault();

			var
				form = $(this),
				id = form.find('.form-container__id').val(),
				url = form.attr('action') + id,
				formData = new FormData();

			form.find('[name]').not('[type=submit]').each(function () {
				var $this = $(this),
					name = $this.attr('name'),
					val = $this.val();

				formData.append(name, val);
			});

			formData.append('_token', _token);

			if (validation.check(form)) {
				$.ajax({
					url : url,
					data : formData,
					type : 'POST',
					dataType: 'json',
					processData: false,
					contentType: false,
					beforeSend : function () {
						form.closest('.popup').addClass('preload');
					},
					success : function (data) {
						var savedData = {},
							item = $('.projects_albums .projects__item').filter('[data-id=' + id + ']');

						savedData.name = form.find('[name=name]').val();
						savedData.description = form.find('[name=description]').val();

						item.find('.project-extra__desc a').text(savedData.name);
						item.find('.project-extra__icon').attr({
							'data-params' : 'description=' + savedData.description + '&id=' + id + '&name=' + savedData.name
						});

						if (data.result) {
							modals.alert(data.result);
						} else {
							modals.alert(data.status);
						}
					}
				})
			}
		});
	}
}