var header = require('./modules/header');
var loadMoreBtn = require('./modules/load-more');
var stickyAddBtn = require('./modules/stick-plus');
var albumHeader = require('./modules/albumHeader');
var album = require('./modules/album');
var fileUpload = require('./modules/fileupload');
var photo = require('./modules/photo');
var fullView = require('./modules/full-info-view');
var triggers = require('./modules/triggers');

$(document).ready(function(){
	var wScroll = $(window).scrollTop();

	//header
	header.edit();
	header.cancel();
	header.changeAvatar();
	header.changeBg();
	header.socials();

	header.saveAlbumHeader(); // header на внутренних
	header.saveMainHeader(); // header на главной

	//Загрузить еще
	loadMoreBtn.load();

	//прилипающие плюсики (добавления)
	stickyAddBtn.stickOnLoad(wScroll);

	//все что связано с альбомом
	albumHeader.stickOnLoad(wScroll);
	album.save();
	album.showAddModal(); // так же для появления редактирования фото
	album.edit();
	album.remove();
	album.addNew();

	//загрузчик файлов
	fileUpload.showPopup();
	fileUpload.init();

	//удаление фото (редактирование фото - в модуле альбома)
	photo.remove();

	//большая всплывашка, для просмотра альбома + для комментов и лайков
	fullView.view();
	fullView.comments();
	fullView.likes();

	//разные кнопки
	triggers.loginBtn();
	triggers.upBtn();
}); // -> ready_end;


$(window).scroll(function () {
	var wScroll = $(window).scrollTop();

	//прилипающий плюсик
	stickyAddBtn.stickOnScroll(wScroll);

	//прилипание строки инфы по альбому (лайки, комменты, комментарии)
	albumHeader.stickOnScroll(wScroll);
});